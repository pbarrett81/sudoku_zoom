package com.myprojects.android.sudokuzoom;

import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity
        implements SelectTimedDialogFragment.Callback,
        SelectLayersDialogFragment.Callback,
        NoResponseDialogFragment.Callback,
        PuzzleView.Callback,
        ClearAllDialogFragment.Callback,
        CellPopupWindow.Callback,
        GameLayout.Callback,
        SubmitAnswerDialogFragment.Callback,
        NewBestTimeDialogFragment.Callback,
        SoloGameFragment.Callback {

    public static final String LOG_TAG = MainActivity.class.getSimpleName();

    public static final String ACTION_GENERATE_GAME = "com.myprojects.android.sudokuzoom.GenerateGame";
    public static final String ACTION_LOAD_GAME = "com.myprojects.android.sudokuzoom.LoadGame";
    public static final String ACTION_SAVE_GAME_PUZZLE_INFO = "com.myprojects.android.sudokuzoom.SaveGamePuzzleInfo";
    public static final String ACTION_SAVE_GAME_INFO = "com.myprojects.android.sudokuzoom.SaveGameInfo";
    public static final String ACTION_DB_INSERT = "com.myprojects.android.sudokuzoom.DBInsert";
    public static final String ACTION_DB_DELETE = "com.myprojects.android.sudokuzoom.DBDelete";
    public static final String ACTION_DB_UPDATE = "com.myprojects.android.sudokuzoom.DBUpdate";
    public static final String ACTION_NONE = "com.myprojects.android.sudokuzoom.None";

    public static final String NOTIFY_GENERATE_LAYER = "com.myprojects.android.sudokuzoom.NotifyGenerateLayer";
    public static final String UPDATE_GENERATE_PROGRESS = "com.myprojects.android.sudokuzoom.UpdateGenerateProgress";
    public static final String SAVING_GAME = "com.myprojects.android.sudokuzoom.SavingGame";
    public static final String GENERATE_WORK_FINISHED = "com.myprojects.android.sudokuzoom.GenerateWorkFinished";

    public static final String SAVE_GAME_PUZZLE_INFO_FINISHED = "com.myprojects.android.sudokuzoom.SaveGamePuzzleInfoFinished";
    public static final String SAVE_GAME_INFO_FINISHED = "com.myprojects.android.sudokuzoom.SaveGameInfoFinished";

    public static final String NOTIFY_LOAD_TOTAL = "com.myprojects.android.sudokuzoom.LoadTotal";
    public static final String UPDATE_LOAD_PROGRESS = "com.myprojects.android.sudokuzoom.UpdateLoadProgress";
    public static final String LOAD_WORK_FINISHED = "com.myprojects.android.sudokuzoom.LoadWorkFinished";

    public static final String STOP_WORK = "com.myprojects.android.sudokuzoom.StopWork";
    public static final String CONFIG_CHANGE = "com.myprojects.android.sudokuzoom.ConfigChange";

    private final String SELECT_LAYERS_FRAGMENT_TAG = "select_layers";
    private final String SELECT_TIMED_FRAGMENT_TAG = "select_timed";
    private final String NO_RESPONSE_FRAGMENT_TAG = "no_response";
    private final String SOLO_GAME_FRAGMENT_TAG = "solo_game";
    private final String CLEAR_ALL_DIALOG_FRAGMENT_TAG = "clear_all_dialog";
    private final String SUBMIT_ANSWER_DIALOG_FRAGMENT_TAG = "submit_answer_dialog";
    private final String NEW_RECORD_DIALOG_FRAGMENT_TAG = "new_record_dialog";
    private final String BEST_TIMES_DIALOG_FRAGMENT_TAG = "best_times_dialog";
    private final String ABOUT_DIALOG_FRAGMENT_TAG = "about_dialog";

    private FragmentManager fm = getSupportFragmentManager();

    private GameTimer mCurrGameTimer;
    private GameTimer mNewGameTimer;

    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mLocalBroadcastManager;

    private String mServiceRunning = ACTION_NONE;

    private MainActivityModel mMainActivityModel;

    private LinearLayout mRootLayout;

    private boolean mCurrentTimerSet = false;
    private boolean mTimedGameSaved = false;
    private boolean mFirstRun = false;

    private TextView mCorrectMessage;
    private TextView mIncorrectMessage;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        mRootLayout = (LinearLayout) findViewById(R.id.root_layout);

        Resources resources = getResources();

        mCorrectMessage = (TextView) LayoutInflater.from(this).inflate(R.layout.textview_feedback, null);
        ViewCompat.setBackground(
                mCorrectMessage,
                ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.background_correct_message,
                        null
                )
        );
        mCorrectMessage.setText(getString(R.string.correct_message));

        mIncorrectMessage = (TextView) LayoutInflater.from(this).inflate(R.layout.textview_feedback, null);
        ViewCompat.setBackground(
                mIncorrectMessage,
                ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.background_incorrect_message,
                        null
                )
        );
        mIncorrectMessage.setText(getString(R.string.incorrect_message));

        initBroadcast();

        mMainActivityModel = ViewModelProviders.of(this).get(MainActivityModel.class);

        if (savedInstanceState != null) {
            mServiceRunning = savedInstanceState.getString("SERVICE_RUNNING");
            mFirstRun = savedInstanceState.getBoolean("FIRST_RUN");

            if (mServiceRunning != null) {
                mNewGameTimer = mMainActivityModel.getNewTimer();
                mCurrGameTimer = mMainActivityModel.getCurrTimer();

                if (mServiceRunning.equals(ACTION_NONE)) {

                    if (mNewGameTimer != null) {
                        mCurrentTimerSet = true;
                    }
                } else {

                    if (mCurrGameTimer.getStartTime() > -1) {
                        mCurrentTimerSet = true;
                    }
                }
            }
        }

        SoloGameFragment soloGameFragment = new SoloGameFragment();
        Bundle args = new Bundle();
        args.putString("ACTION", mServiceRunning);
        soloGameFragment.setArguments(args);
        replaceFragment(soloGameFragment, SOLO_GAME_FRAGMENT_TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            case R.id.create_new_game:
                createNewGame();
                return true;

            case R.id.load_timed_game:
                loadTimedGame();
                return true;

            case R.id.load_untimed_game:
                loadUntimedGame();
                return true;

            case R.id.tutorial:
                startActivity(new Intent(this, TutorialActivity.class));
                return true;

            case R.id.best_times:
                BestTimesDialogFragment bestTimesDialogFragment = new BestTimesDialogFragment();
                bestTimesDialogFragment.show(fm, BEST_TIMES_DIALOG_FRAGMENT_TAG);
                return true;

            case R.id.about:
                AboutDialogFragment aboutDialogFragment = new AboutDialogFragment();
                aboutDialogFragment.show(fm, ABOUT_DIALOG_FRAGMENT_TAG);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        MenuItem createNewGameItem = menu.findItem(R.id.create_new_game);
        MenuItem loadTimedGameItem = menu.findItem(R.id.load_timed_game);
        MenuItem loadUntimedGameItem = menu.findItem(R.id.load_untimed_game);

        if (!mServiceRunning.equals(ACTION_NONE)) {
            createNewGameItem.setEnabled(false);
            loadTimedGameItem.setEnabled(false);
            loadUntimedGameItem.setEnabled(false);
        } else {
            createNewGameItem.setEnabled(true);

            DBQueryTask dbQueryTask = new DBQueryTask();
            dbQueryTask.execute(
                    new DBQueryTask.Data(
                            this,
                            LocalContract.GameInfoEntry.CONTENT_URI,
                            null,
                            LocalContract.GameInfoEntry.COLUMN_START_TIME + " > 0",
                            null,
                            null
                    )
            );

            Cursor cursor = null;

            try {
                cursor = dbQueryTask.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            if (cursor != null) {

                if (cursor.getCount() > 0) {

                    if (mCurrGameTimer != null) {

                        if (mCurrGameTimer.getStartTime() > 0) {
                            loadTimedGameItem.setEnabled(false);
                        } else {
                            loadTimedGameItem.setEnabled(true);
                        }
                    } else {
                        loadTimedGameItem.setEnabled(true);
                    }
                } else {
                    loadTimedGameItem.setEnabled(false);
                }

                cursor.close();
            } else {
                loadTimedGameItem.setEnabled(false);
            }

            dbQueryTask = new DBQueryTask();
            dbQueryTask.execute(
                    new DBQueryTask.Data(
                            this,
                            LocalContract.GameInfoEntry.CONTENT_URI,
                            null,
                            LocalContract.GameInfoEntry.COLUMN_START_TIME + " = 0",
                            null,
                            null
                    )
            );

            cursor = null;

            try {
                cursor = dbQueryTask.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            if (cursor != null) {

                if (cursor.getCount() > 0) {

                    if (mCurrGameTimer != null) {

                        if (mCurrGameTimer.getStartTime() == 0) {
                            loadUntimedGameItem.setEnabled(false);
                        } else {
                            loadUntimedGameItem.setEnabled(true);
                        }
                    } else {
                        loadUntimedGameItem.setEnabled(true);
                    }
                } else {
                    loadUntimedGameItem.setEnabled(false);
                }

                cursor.close();
            } else {
                loadUntimedGameItem.setEnabled(false);
            }
        }

        SoloGameFragment soloGameFragment = findGameFragment();

        if (soloGameFragment != null) {
            soloGameFragment.dismissCellPopup();
        }

        return true;
    }

    @Override
    public void onResumeFragments() {
        super.onResumeFragments();

        if (mFirstRun) {
            createNewGame();
        } else if (mMainActivityModel.getPuzzleDataArray() == null && mServiceRunning.equals(ACTION_NONE)) {
            start();
        }
    }

    @Override
    public void onSoloGameFragmentDisplayed() {

        if (mCurrentTimerSet && mMainActivityModel.getPuzzleDataArray() != null) {
            SoloGameFragment soloGameFragment = findGameFragment();
            soloGameFragment.initGameLayout(mCurrGameTimer, false);
        }
    }

    public void initBroadcast() {
        mBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                SoloGameFragment soloGameFragment = findGameFragment();
                String action = intent.getAction();

                if (action != null) {

                    if (action.equals(GENERATE_WORK_FINISHED)) {
                        mServiceRunning = ACTION_NONE;

                        if (!intent.getBooleanExtra("com.myprojects.android.sudokuzoom.WorkStopped", true)) {
                            ArrayList<PuzzleData> puzzleDataList = intent.getParcelableArrayListExtra(
                                    "com.myprojects.android.sudokuzoom.PuzzleDataList"
                            );
                            PuzzleData[] puzzleDataArray = new PuzzleData[puzzleDataList.size()];

                            for (int i = 0; i < puzzleDataArray.length; ++i) {
                                puzzleDataArray[i] = puzzleDataList.get(i);
                            }

                            mMainActivityModel.setPuzzleDataArray(puzzleDataArray);

                            Intent newIntent = new Intent(MainActivity.ACTION_SAVE_GAME_PUZZLE_INFO);
                            newIntent.putExtra(
                                    "com.myprojects.android.sudokuzoom.Timed",
                                    mNewGameTimer.getStartTime() > 0
                            );
                            newIntent.putParcelableArrayListExtra(
                                    "com.myprojects.android.sudokuzoom.PuzzleDataList",
                                    puzzleDataList
                            );

                            DatabaseService.enqueueWork(getApplicationContext(), newIntent);

                            mServiceRunning = ACTION_SAVE_GAME_PUZZLE_INFO;
                        }
                    } else if (action.equals(SAVE_GAME_PUZZLE_INFO_FINISHED)) {
                        mServiceRunning = ACTION_NONE;

                        int topPuzzleId = mMainActivityModel.getPuzzleDataArray()[0].getPuzzleId();

                        if (mNewGameTimer.getStartTime() > 0) {
                            mNewGameTimer.addElapsedToStart();
                            launchSaveGameInfoService(topPuzzleId);
                        } else {
                            launchSaveGameInfoService(topPuzzleId);
                        }

                    } else if (action.equals(SAVE_GAME_INFO_FINISHED)) {
                        mServiceRunning = ACTION_NONE;

                        if (mCurrGameTimer != null) {
                            mCurrGameTimer.stop();
                        }

                        mCurrGameTimer = mNewGameTimer;
                        mCurrentTimerSet = true;

                        if (soloGameFragment.isCreated()) {
                            soloGameFragment.setCurrentAction(ACTION_NONE);
                            soloGameFragment.setDisplay();
                            soloGameFragment.initGameLayout(mCurrGameTimer, true);
                            soloGameFragment.toggleSavingGameVisibility(View.INVISIBLE);
                        }
                    } else if (action.equals(LOAD_WORK_FINISHED)) {
                        mServiceRunning = ACTION_NONE;

                        if (!intent.getBooleanExtra("com.myprojects.android.sudokuzoom.WorkStopped", true)) {
                            ArrayList<Parcelable> parcelables = intent.getParcelableArrayListExtra(
                                    "com.myprojects.android.sudokuzoom.PuzzleDataList"
                            );
                            PuzzleData[] puzzleDataArray = new PuzzleData[parcelables.size()];

                            for (int i = 0; i < puzzleDataArray.length; ++i) {
                                puzzleDataArray[i] = (PuzzleData) parcelables.get(i);
                            }

                            mMainActivityModel.setPuzzleDataArray(puzzleDataArray);

                            if (mCurrGameTimer != null) {
                                mCurrGameTimer.stop();
                            }

                            mCurrGameTimer = mNewGameTimer;
                            mCurrentTimerSet = true;

                            if (soloGameFragment.isCreated()) {
                                soloGameFragment.setCurrentAction(ACTION_NONE);
                                soloGameFragment.initGameLayout(mCurrGameTimer, false);
                                soloGameFragment.toggleProgressBarVisibility(View.INVISIBLE);
                            }
                        }
                    }
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(GENERATE_WORK_FINISHED);
        intentFilter.addAction(SAVE_GAME_PUZZLE_INFO_FINISHED);
        intentFilter.addAction(SAVE_GAME_INFO_FINISHED);
        intentFilter.addAction(LOAD_WORK_FINISHED);

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
    }

    public void broadcast(String action) {
        Intent broadcastIntent = new Intent(action);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    public void start() {

        if (!loadTimedGame()) {

            if (!loadUntimedGame() && !mTimedGameSaved) {
                mFirstRun = true;
                startActivity(new Intent(this, TutorialActivity.class));
            }
        }
    }

    public void createNewGame() {
        List<Fragment> fragments = fm.getFragments();
        boolean dialogShown = false;

        for (int i = 0; i < fragments.size(); ++i) {
            Fragment fragment = fragments.get(i);

            if (fragment instanceof SelectTimedDialogFragment || fragment instanceof SelectLayersDialogFragment) {
                dialogShown = true;
            }
        }

        if (!dialogShown) {
            String timeFromInternet = getTimeFromInternet();
            mNewGameTimer = new GameTimer();

            if (timeFromInternet != null) {

                if (!timeFromInternet.equals("-1")) {
                    mNewGameTimer.setStartTime(Long.parseLong(timeFromInternet));
                    mNewGameTimer.start();

                    SelectTimedDialogFragment selectTimedDialogFragment = new SelectTimedDialogFragment();
                    selectTimedDialogFragment.show(fm, SELECT_TIMED_FRAGMENT_TAG);
                } else {
                    mNewGameTimer.setStartTime(0);
                    createNoResponseDialog(NoResponseDialogFragment.GENERATE_DIALOG);
                }
            } else {
                mNewGameTimer.setStartTime(0);
                createNoResponseDialog(NoResponseDialogFragment.GENERATE_DIALOG);
            }
        }
    }

    public boolean loadTimedGame() {
        DBQueryTask dbQueryTask = new DBQueryTask();
        dbQueryTask.execute(
                new DBQueryTask.Data(
                        this,
                        LocalContract.GameInfoEntry.CONTENT_URI,
                        null,
                        LocalContract.GameInfoEntry.COLUMN_START_TIME + " > 0",
                        null,
                        null
                )
        );

        Cursor cursor = null;

        try {
            cursor = dbQueryTask.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (cursor != null) {
            cursor.moveToFirst();

            if (cursor.getCount() > 0) {
                mTimedGameSaved = true;
                long startTime = Long.parseLong(cursor.getString(
                        cursor.getColumnIndex(
                                LocalContract.GameInfoEntry.COLUMN_START_TIME
                        )
                ));
                String currentTime = getTimeFromInternet();

                if (currentTime != null) {

                    if (!currentTime.equals("-1")) {
                        mNewGameTimer = new GameTimer();
                        mNewGameTimer.setStartTime(startTime);
                        mNewGameTimer.setElapsedTime(Long.parseLong(currentTime) - startTime);
                        mNewGameTimer.start();

                        mMainActivityModel.setCurrentPuzzleId(
                                Integer.valueOf(
                                        cursor.getString(
                                                cursor.getColumnIndex(
                                                        LocalContract.GameInfoEntry.COLUMN_CURRENT_PUZZLE_ID
                                                )
                                        )
                                )
                        );

                        launchLoadGameService();
                        cursor.close();
                        return true;
                    } else {
                        createNoResponseDialog(NoResponseDialogFragment.LOAD_DIALOG);
                    }
                } else {
                    createNoResponseDialog(NoResponseDialogFragment.LOAD_DIALOG);
                }
            }

            cursor.close();
        }

        return false;
    }

    public boolean loadUntimedGame() {
        DBQueryTask dbQueryTask = new DBQueryTask();
        dbQueryTask.execute(
                new DBQueryTask.Data(
                        this,
                        LocalContract.GameInfoEntry.CONTENT_URI,
                        null,
                        LocalContract.GameInfoEntry.COLUMN_START_TIME + " = 0",
                        null,
                        null
                )
        );

        Cursor cursor = null;

        try {
            cursor = dbQueryTask.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (cursor != null) {
            cursor.moveToFirst();

            if (cursor.getCount() > 0) {
                mNewGameTimer = new GameTimer();

                mMainActivityModel.setCurrentPuzzleId(
                        Integer.valueOf(
                                cursor.getString(
                                        cursor.getColumnIndex(
                                                LocalContract.GameInfoEntry.COLUMN_CURRENT_PUZZLE_ID
                                        )
                                )
                        )
                );

                initUntimedGame();
                launchLoadGameService();
                cursor.close();
                return true;
            }
        }

        return false;
    }

    public String getTimeFromInternet() {
        String time;
        HttpDateTask httpDateTask = new HttpDateTask();
        httpDateTask.execute();

        try {
            time = httpDateTask.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            time = null;
        } catch (ExecutionException e) {
            e.printStackTrace();
            time = null;
        }

        return time;
    }

    public void createNoResponseDialog(int type) {
        NoResponseDialogFragment noResponseDialogFragment = new NoResponseDialogFragment();
        Bundle args = new Bundle();
        args.putInt("DIALOG", type);
        noResponseDialogFragment.setArguments(args);
        noResponseDialogFragment.show(fm, NO_RESPONSE_FRAGMENT_TAG);
    }

    public void launchGenerateGameService(int numLayers) {
        mServiceRunning = ACTION_GENERATE_GAME;
        mCurrentTimerSet = false;

        SoloGameFragment soloGameFragment = findGameFragment();
        soloGameFragment.setCurrentAction(mServiceRunning);
        soloGameFragment.setDisplay();

        Intent intent = new Intent();
        intent.putExtra("com.myprojects.android.sudokuzoom.NumLayers", numLayers);
        intent.putExtra("com.myprojects.android.sudokuzoom.Timed", mNewGameTimer.getStartTime() > 0);

        GenerateGameService.enqueueWork(getApplicationContext(), intent);
        //GenerateGameService.enqueueWork(this, intent);
    }

    public void launchSaveGameInfoService(int topPuzzleId) {
        Intent intent = new Intent(ACTION_SAVE_GAME_INFO);
        intent.putExtra("com.myprojects.android.sudokuzoom.StartTime", mNewGameTimer.getStartTime());
        intent.putExtra("com.myprojects.android.sudokuzoom.TopPuzzleId", topPuzzleId);

        DatabaseService.enqueueWork(getApplicationContext(), intent);
        //DatabaseService.enqueueWork(this, intent);

        mServiceRunning = ACTION_SAVE_GAME_INFO;
    }

    public void launchLoadGameService() {
        mServiceRunning = ACTION_LOAD_GAME;
        mCurrentTimerSet = false;

        SoloGameFragment soloGameFragment = findGameFragment();
        soloGameFragment.setCurrentAction(mServiceRunning);
        soloGameFragment.setDisplay();

        Intent intent = new Intent(ACTION_LOAD_GAME);
        intent.putExtra(
                "com.myprojects.android.sudokuzoom.Timed",
                mNewGameTimer.getStartTime() > 0
        );

        DatabaseService.enqueueWork(getApplicationContext(), intent);
        //DatabaseService.enqueueWork(this, intent);

    }

    @Override
    public void onTimedOkSelected(boolean timed) {

        if (!timed) {
            initUntimedGame();
        }

        SelectLayersDialogFragment selectLayersDialogFragment = new SelectLayersDialogFragment();
        selectLayersDialogFragment.show(fm, SELECT_LAYERS_FRAGMENT_TAG);
    }

    @Override
    public void onLayersOkSelected(int numLayers) {
        mFirstRun = false;
        mMainActivityModel.setCurrentPuzzleId(0);
        launchGenerateGameService(numLayers);
    }

    @Override
    public void onNoResponseOkSelected() {
        onTimedOkSelected(false);
    }

    public void initUntimedGame() {

        if (mNewGameTimer != null) {
            mNewGameTimer.setStartTime(0);
            mNewGameTimer.stop();
        }
    }

    public SoloGameFragment findGameFragment() {
        return (SoloGameFragment) fm.findFragmentByTag(SOLO_GAME_FRAGMENT_TAG);
    }

    @Override
    public void onPuzzleZoomIn(int section) {
        SoloGameFragment gameFragment = findGameFragment();
        gameFragment.getGameLayout().zoomInPuzzle(section, GameLayout.DEFAULT_DURATION);
    }

    @Override
    public void onPuzzleZoomOut() {
        SoloGameFragment gameFragment = findGameFragment();
        gameFragment.getGameLayout().zoomOutPuzzle();
    }

    @Override
    public void onCellClicked(CellLayout cellLayout, int section) {
        SoloGameFragment gameFragment = findGameFragment();
        gameFragment.getGameLayout().onCellClicked(cellLayout, section);
    }

    @Override
    public void onPopupOkPressed(int num) {
        SoloGameFragment gameFragment = findGameFragment();

        if (gameFragment != null) {
            GameLayout gameLayout = gameFragment.getGameLayout();

            if (gameLayout != null) {

                if (num == 0) {
                    gameLayout.onClearKeyPressed();
                } else {
                    gameLayout.onNumKeyPressed(num);
                }
            }
        }
    }

    @Override
    public void onClearAllKeyPressed() {
        ClearAllDialogFragment clearAllDialogFragment = new ClearAllDialogFragment();
        clearAllDialogFragment.show(fm, CLEAR_ALL_DIALOG_FRAGMENT_TAG);
    }

    @Override
    public void onConfirmYesPressed() {
        SoloGameFragment gameFragment = findGameFragment();
        gameFragment.getGameLayout().onConfirmYesPressed();
    }

    @Override
    public void onSubmitAnswerKeyPressed() {
        SoloGameFragment gameFragment = findGameFragment();
        gameFragment.getGameLayout().onSubmitAnswerKeyPressed();
    }

    @Override
    public void onAnswerChecked(boolean correct, boolean topPuzzle) {

        if (topPuzzle) {
            SubmitAnswerDialogFragment submitAnswerDialogFragment = new SubmitAnswerDialogFragment();
            Bundle args = new Bundle();
            args.putBoolean("CORRECT", correct);
            args.putLong("ELAPSED_TIME", mCurrGameTimer.getElapsedTime());
            submitAnswerDialogFragment.setArguments(args);
            submitAnswerDialogFragment.show(fm, SUBMIT_ANSWER_DIALOG_FRAGMENT_TAG);
        } else {
            Toast toast = new Toast(this);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);

            if (correct) {
                toast.setView(mCorrectMessage);
                onCorrectOkPressed();
            } else {
                toast.setView(mIncorrectMessage);
            }

            toast.show();
        }
    }

    @Override
    public void onCorrectOkPressed() {
        SoloGameFragment gameFragment = findGameFragment();
        gameFragment.getGameLayout().onCorrectOkPressed();
    }

    @Override
    public void onComplete(int numLayers) {

        if (mCurrGameTimer.getStartTime() > 0) {
            DBQueryTask dbQueryTask = new DBQueryTask();
            dbQueryTask.execute(
                    new DBQueryTask.Data(
                            this,
                            LocalContract.BestTimesEntry.CONTENT_URI,
                            null,
                            LocalContract.BestTimesEntry.COLUMN_LAYER_COUNT + " = ?",
                            new String[]{String.valueOf(numLayers)},
                            null
                    )
            );

            Cursor cursor = null;

            try {
                cursor = dbQueryTask.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            if (cursor != null) {
                long elapsedTime = mCurrGameTimer.getElapsedTime();

                cursor.moveToFirst();

                if (cursor.getCount() > 0) {
                    String strCurrTime = cursor.getString(
                            cursor.getColumnIndex(
                                    LocalContract.BestTimesEntry.COLUMN_COMPLETION_TIME
                            )
                    );

                    if (elapsedTime < Long.parseLong(strCurrTime)) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(
                                LocalContract.BestTimesEntry.COLUMN_COMPLETION_TIME,
                                String.valueOf(elapsedTime)
                        );
                        contentValues.put(
                                LocalContract.BestTimesEntry.COLUMN_DATE,
                                String.valueOf(mCurrGameTimer.getStartTime() + elapsedTime)
                        );

                        Intent intent = new Intent(MainActivity.ACTION_DB_UPDATE);
                        intent.putExtra(
                                "com.myprojects.android.sudokuzoom.Uri",
                                LocalContract.BestTimesEntry.buildBestTimesUri((long) numLayers)
                        );
                        intent.putExtra("com.myprojects.android.sudokuzoom.ContentValues", contentValues);
                        intent.putExtra("com.myprojects.android.sudokuzoom.Selection", "");
                        intent.putExtra("com.myprojects.android.sudokuzoom.SelectionArgs", new String[]{});

                        DatabaseService.enqueueWork(getApplicationContext(), intent);
                        //DatabaseService.enqueueWork(this, intent);

                        createNewBestTimeDialogFragment(
                                numLayers,
                                strCurrTime,
                                cursor.getString(
                                        cursor.getColumnIndex(
                                                LocalContract.BestTimesEntry.COLUMN_DATE
                                        )
                                ),
                                cursor.getString(
                                        cursor.getColumnIndex(
                                                LocalContract.BestTimesEntry.COLUMN_PLAYER_NAME
                                        )
                                )
                        );
                    } else {
                        // Not a new record.
                    }
                } else {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(
                            LocalContract.BestTimesEntry.COLUMN_LAYER_COUNT,
                            String.valueOf(numLayers));
                    contentValues.put(
                            LocalContract.BestTimesEntry.COLUMN_COMPLETION_TIME,
                            String.valueOf(elapsedTime));
                    contentValues.put(
                            LocalContract.BestTimesEntry.COLUMN_DATE,
                            String.valueOf(mCurrGameTimer.getStartTime() + elapsedTime)
                    );

                    Intent intent = new Intent(MainActivity.ACTION_DB_INSERT);
                    intent.putExtra(
                            "com.myprojects.android.sudokuzoom.Uri",
                            LocalContract.BestTimesEntry.CONTENT_URI
                    );
                    intent.putExtra("com.myprojects.android.sudokuzoom.ContentValues", contentValues);

                    DatabaseService.enqueueWork(getApplicationContext(), intent);

                    createNewBestTimeDialogFragment(
                            numLayers,
                            "",
                            "",
                            ""
                    );
                }

                cursor.close();
            }
        }
    }

    @Override
    public void onNewBestTimeDonePressed(String playerName, int numLayers) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(LocalContract.BestTimesEntry.COLUMN_PLAYER_NAME, playerName);

        Intent intent = new Intent(MainActivity.ACTION_DB_UPDATE);
        intent.putExtra(
                "com.myprojects.android.sudokuzoom.Uri",
                LocalContract.BestTimesEntry.buildBestTimesUri((long) numLayers)
        );
        intent.putExtra("com.myprojects.android.sudokuzoom.ContentValues", contentValues);
        intent.putExtra("com.myprojects.android.sudokuzoom.Selection", "");
        intent.putExtra("com.myprojects.android.sudokuzoom.SelectionArgs", new String[]{});

        DatabaseService.enqueueWork(getApplicationContext(), intent);
    }

    @Override
    public void onCancelButtonClicked() {
        broadcast(STOP_WORK);
        mServiceRunning = ACTION_NONE;
    }

    public void createNewBestTimeDialogFragment(int numLayers, String time, String date, String playerName) {
        NewBestTimeDialogFragment newBestTimeDialogFragment = new NewBestTimeDialogFragment();
        Bundle args = new Bundle();

        args.putInt("NUM_LAYERS", numLayers);
        args.putLong("NEW_COMPLETION_TIME", mCurrGameTimer.getElapsedTime());
        args.putLong("NEW_DATE", mCurrGameTimer.getStartTime() + mCurrGameTimer.getElapsedTime());
        args.putString("PREV_COMPLETION_TIME", time);
        args.putString("PREV_DATE", date);
        args.putString("PREV_PLAYER_NAME", playerName);

        newBestTimeDialogFragment.setArguments(args);
        newBestTimeDialogFragment.show(fm, NEW_RECORD_DIALOG_FRAGMENT_TAG);
    }

    public void replaceFragment(Fragment fragment, String tag) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.main_container, fragment, tag);
        ft.commit();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("SERVICE_RUNNING", mServiceRunning);
        savedInstanceState.putBoolean("FIRST_RUN", mFirstRun);

        mMainActivityModel.setNewTimer(mNewGameTimer);
        mMainActivityModel.setCurrTimer(mCurrGameTimer);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocalBroadcastManager.unregisterReceiver(mBroadcastReceiver);
        mLocalBroadcastManager = null;
        mBroadcastReceiver = null;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_DEL || keyCode == KeyEvent.KEYCODE_FORWARD_DEL) {
            onPopupOkPressed(0);
        } else {
            String name = KeyEvent.keyCodeToString(keyCode);
            String character = name.substring(8, name.length());

            if (character.matches("[1-9]")) {
                onPopupOkPressed(Integer.valueOf(character));
            }
        }

        return super.onKeyUp(keyCode, event);
    }
}