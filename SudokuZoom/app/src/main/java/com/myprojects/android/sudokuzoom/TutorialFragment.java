package com.myprojects.android.sudokuzoom;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Owner on 3/1/2018.
 */

public class TutorialFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    public TutorialFragment() {
    }

    public static TutorialFragment newInstance(int sectionNumber) {
        TutorialFragment fragment = new TutorialFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tutorial, container, false);
        TextView sectionLabel = (TextView) rootView.findViewById(R.id.section_label);
        TextView sectionText = (TextView) rootView.findViewById(R.id.section_text);

        Bundle args = getArguments();

        if (args != null) {

            switch (args.getInt(ARG_SECTION_NUMBER)) {

                case 0:
                    sectionLabel.setText(getString(R.string.tutorial_section_label_0));
                    sectionText.setText(Html.fromHtml(getString(R.string.tutorial_section_text_0)));
                    break;

                case 1:
                    sectionLabel.setText(getString(R.string.tutorial_section_label_1));
                    sectionText.setText(Html.fromHtml(getString(R.string.tutorial_section_text_1)));
                    break;

                case 2:
                    sectionLabel.setText(getString(R.string.tutorial_section_label_2));
                    sectionText.setText(Html.fromHtml(getString(R.string.tutorial_section_text_2)));
                    break;

                case 3:
                    sectionLabel.setText(getString(R.string.tutorial_section_label_3));
                    sectionText.setText(Html.fromHtml(getString(R.string.tutorial_section_text_3)));
                    break;

                case 4:
                    sectionLabel.setText(getString(R.string.tutorial_section_label_4));
                    sectionText.setText(Html.fromHtml(getString(R.string.tutorial_section_text_4)));
                    break;

                case 5:
                    sectionLabel.setText(getString(R.string.tutorial_section_label_5));
                    sectionText.setText(Html.fromHtml(getString(R.string.tutorial_section_text_5)));
                    break;

                case 6:
                    sectionLabel.setText(getString(R.string.tutorial_section_label_6));
                    sectionText.setText(Html.fromHtml(getString(R.string.tutorial_section_text_6)));
                    break;

                case 7:
                    sectionLabel.setText(getString(R.string.tutorial_section_label_7));
                    sectionText.setText(Html.fromHtml(getString(R.string.tutorial_section_text_7)));
                    break;

                case 8:
                    sectionLabel.setText(getString(R.string.tutorial_section_label_8));
                    sectionText.setText(Html.fromHtml(getString(R.string.tutorial_section_text_8)));
                    break;
            }
        }

        return rootView;
    }
}
