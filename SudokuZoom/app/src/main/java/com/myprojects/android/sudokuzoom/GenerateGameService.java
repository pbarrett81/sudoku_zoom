package com.myprojects.android.sudokuzoom;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;

/**
 * Created by Owner on 1/27/2018.
 */

public class GenerateGameService extends JobIntentService {

    /**
     * Unique job ID for this service.
     */
    private static final int JOB_ID = 1000;

    public static boolean mRunning = false;
    public static int mLayerNum = 1;
    public static int mTotalPuzzles = 1;
    public static int mPuzzleCount = 0;

    private BroadcastReceiver mBroadcastReceiver;

    private int mNumLayers;
    private boolean mStopWork = false;
    private ArrayList<PuzzleData> mPuzzleDataList;

    private int mLayersGenerated;
    private int mIdCount;

    private boolean mTimed = false;

    /**
     * Convenience method for enqueuing work in to this service.
     */
    static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, GenerateGameService.class, JOB_ID, work);
    }

    public GenerateGameService() {
        super();

        resetProgress();

        mBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(MainActivity.STOP_WORK)) {
                    mStopWork = true;
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MainActivity.STOP_WORK);

        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, intentFilter);
    }

    @Override
    public void onHandleWork (@NonNull Intent intent) {
        mRunning = true;

        mNumLayers = intent.getIntExtra("com.myprojects.android.sudokuzoom.NumLayers", 0);
        mTimed = intent.getBooleanExtra("com.myprojects.android.sudokuzoom.Timed", false);

        if (mNumLayers > 0 && !mStopWork) {
            generateGame();
        }

        Intent broadcastIntent = new Intent(MainActivity.GENERATE_WORK_FINISHED);
        broadcastIntent.putExtra("com.myprojects.android.sudokuzoom.WorkStopped", mStopWork);
        broadcastIntent.putParcelableArrayListExtra(
                "com.myprojects.android.sudokuzoom.PuzzleDataList",
                mPuzzleDataList
        );

        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);

        mStopWork = false;
        mRunning = false;
        resetProgress();
    }

    public void generateGame() {
        ArrayList<PuzzleData> currentLayer = new ArrayList<>();
        ArrayList<PuzzleData> nextLayer = new ArrayList<>();
        mPuzzleDataList = new ArrayList<>();
        mIdCount = 0;

        currentLayer.add(generatePuzzleData());
        mLayersGenerated = 1;
        mPuzzleCount = 1;
        updateProgress();

        stopWork:
        {
            while (mLayersGenerated < mNumLayers) {

                int totalPuzzles = 0;

                for (PuzzleData puzzleData : currentLayer) {

                    for (int j = 0; j < 9; ++j) {

                        if (mStopWork) {
                            break stopWork;
                        }

                        if (!puzzleData.isSectionEmpty(j)) {
                            ++totalPuzzles;
                        }
                    }
                }

                mPuzzleCount = 0;
                notifyLayer(mLayersGenerated + 1, totalPuzzles);

                while (currentLayer.size() > 0) {
                    PuzzleData currPuzzleData = currentLayer.remove(0);

                    for (int j = 0; j < 9; ++j) {

                        if (mStopWork) {
                            break stopWork;
                        }

                        if (!currPuzzleData.isSectionEmpty(j)) {
                            PuzzleData childPuzzleData = generatePuzzleData();
                            currPuzzleData.setChildPuzzleId(childPuzzleData.getPuzzleId(), j);
                            nextLayer.add(childPuzzleData);
                            ++mPuzzleCount;
                            updateProgress();
                        }
                    }
                }

                currentLayer.addAll(nextLayer);
                nextLayer.clear();
                mLayersGenerated++;
            }

            for (PuzzleData puzzleData : currentLayer) {

                if (mStopWork) {
                    break stopWork;
                }

                puzzleData.setGivensToSolved(-1);
            }
        }
    }

    public PuzzleData generatePuzzleData() {
        PuzzleData puzzleData = new PuzzleData(mLayersGenerated + 1, mIdCount);
        boolean isVeryEasyPuzzle = false;

        do {
            isVeryEasyPuzzle = PuzzleGenerator.initPuzzleData(puzzleData);
        } while (!isVeryEasyPuzzle && !mStopWork);

        mPuzzleDataList.add(puzzleData);
        mIdCount++;
        return puzzleData;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    }

    public void notifyLayer(int layerNum, int totalPuzzles) {
        mLayerNum = layerNum;
        mTotalPuzzles = totalPuzzles;

        Intent intent = new Intent(MainActivity.NOTIFY_GENERATE_LAYER);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void updateProgress() {
        Intent intent = new Intent(MainActivity.UPDATE_GENERATE_PROGRESS);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void resetProgress() {
        mLayerNum = 1;
        mTotalPuzzles = 1;
        mPuzzleCount = 0;
    }
}
