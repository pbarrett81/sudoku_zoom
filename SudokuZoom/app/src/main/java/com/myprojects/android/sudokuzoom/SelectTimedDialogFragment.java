package com.myprojects.android.sudokuzoom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;

/**
 * Created by Owner on 11/19/2017.
 */

public class SelectTimedDialogFragment extends DialogFragment {

    private Context mContext;

    private AlertDialog mAlertDialog;

    public interface Callback {
        void onTimedOkSelected(boolean timed);
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.message_new_game);

        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_select_game_type, null);
        final RadioGroup radioGroup = (RadioGroup) contentView.findViewById(R.id.rg);
        builder.setView(contentView);

        builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                int selected = radioGroup.getCheckedRadioButtonId();

                if (selected != -1) {
                    boolean timed = false;

                    switch (selected) {

                        case R.id.radio_1:
                            timed = true;
                            break;

                        case R.id.radio_2:
                            timed = false;
                            break;
                    }

                    ((SelectTimedDialogFragment.Callback) getActivity()).onTimedOkSelected(timed);
                }
            }
        });

        builder.setNegativeButton(R.string.cancel, null);

        mAlertDialog = builder.create();

        return mAlertDialog;
    }
}
