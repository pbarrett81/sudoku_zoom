package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Owner on 5/29/2017.
 */

public class PuzzleViewAdapter extends RecyclerView.Adapter<PuzzleViewAdapter.ViewHolder> {

    public static final String LOG_TAG = PuzzleViewAdapter.class.getSimpleName();

    private Context mContext;

    private ArrayList<ViewHolder> mViewHolders = new ArrayList<>();

    private int mCurrentSection = -1;

    private PuzzleData mPuzzleData;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CellLayout mCellLayout;

        public ViewHolder(CellLayout v) {
            super(v);
            mCellLayout = v;
        }

        public CellLayout getCellLayout() {
            return mCellLayout;
        }
    }

    public PuzzleViewAdapter(Context context, PuzzleData puzzleData) {
        mContext = context;
        mPuzzleData = puzzleData;
    }

    public int getCurrentSection() {
        return mCurrentSection;
    }

    public void setCurrentSection(int currentSection) {
        mCurrentSection = currentSection;
    }

    public PuzzleData getPuzzleData() {
        return mPuzzleData;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PuzzleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CellLayout v = (CellLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_cell, parent, false);
        ViewHolder vh = new ViewHolder(v);
        mViewHolders.add(vh);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CellLayout cellLayout = holder.getCellLayout();
        cellLayout.setCellData(mPuzzleData.getCellDataArray()[position]);

        int givenColor = 0;

        switch (mPuzzleData.getLayerNum()) {

            case 1:
                givenColor = mContext.getResources().getColor(R.color.colorSection1);
                break;

            case 2:
                givenColor = mContext.getResources().getColor(R.color.colorSection2);
                break;

            case 3:
                givenColor = mContext.getResources().getColor(R.color.colorSection3);
                break;
        }

        cellLayout.init(givenColor);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mPuzzleData.getCellDataArray().length;
    }

    public void setStrokeWidths() {
        float strokeWidth = mViewHolders.get(0).mCellLayout.determineStrokeWidth();
        CellData[] cellDataArray = mPuzzleData.getCellDataArray();

        for (int i = 0; i < cellDataArray.length; ++i) {
            mViewHolders.get(i).mCellLayout.setStrokeWidth(strokeWidth);
        }
    }

    public void setTextSizes() {
        float textSize = mViewHolders.get(0).mCellLayout.determineTextSize();
        CellData[] cellDataArray = mPuzzleData.getCellDataArray();

        for (int i = 0; i < cellDataArray.length; ++i) {
            mViewHolders.get(i).mCellLayout.setTextSize(textSize);
        }
    }

    public void clearAllEntries() {

        for (ViewHolder vh : mViewHolders) {
            vh.getCellLayout().clearValue();
        }
    }

    public void setCurrentSectionFromPosition(int position) {
        int[][] sections = SudokuConstants.ADAPTER_SECTIONS;

        if (Utils.searchIntArray(sections[0], position)) {
            mCurrentSection = 0;
        } else if (Utils.searchIntArray(sections[1], position)) {
            mCurrentSection = 1;
        } else if (Utils.searchIntArray(sections[2], position)) {
            mCurrentSection = 2;
        } else if (Utils.searchIntArray(sections[3], position)) {
            mCurrentSection = 3;
        } else if (Utils.searchIntArray(sections[4], position)) {
            mCurrentSection = 4;
        } else if (Utils.searchIntArray(sections[5], position)) {
            mCurrentSection = 5;
        } else if (Utils.searchIntArray(sections[6], position)) {
            mCurrentSection = 6;
        } else if (Utils.searchIntArray(sections[7], position)) {
            mCurrentSection = 7;
        } else if (Utils.searchIntArray(sections[8], position)) {
            mCurrentSection = 8;
        }
    }

    public int[] findCurrentSection() {
        return SudokuConstants.ADAPTER_SECTIONS[mCurrentSection];
    }

    public void setCurrentSectionToSolved() {

        for (ViewHolder vh : mViewHolders) {

            if (Utils.searchIntArray(SudokuConstants.ADAPTER_SECTIONS[mCurrentSection], vh.getAdapterPosition())) {
                vh.getCellLayout().solveGiven();
            }
        }
    }
}