package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by Owner on 7/3/2017.
 */

public class Utils {

    public static final String LOG_TAG = Utils.class.getSimpleName();

    public static boolean searchIntArray(int[] arr, int targetValue) {

        for (int i : arr){

            if (i == targetValue)
                return true;
        }

        return false;
    }

    public static Object removeRandElement(ArrayList list) {
        int listSize = list.size();

        if (listSize > 0) {
            Random rand = new Random();
            return list.remove(rand.nextInt(list.size()));
        } else {
            return null;
        }
    }

    public static ArrayList<String> getCoordinateList() {
        String[] coords = {
                "1,1", "1,2", "1,3", "1,4", "1,5", "1,6,", "1,7", "1,8", "1,9",
                "2,1", "2,2", "2,3", "2,4", "2,5", "2,6,", "2,7", "2,8", "2,9",
                "3,1", "3,2", "3,3", "3,4", "3,5", "3,6,", "3,7", "3,8", "3,9",
                "4,1", "4,2", "4,3", "4,4", "4,5", "4,6,", "4,7", "4,8", "4,9",
                "5,1", "5,2", "5,3", "5,4", "5,5", "5,6,", "5,7", "5,8", "5,9",
                "6,1", "6,2", "6,3", "6,4", "6,5", "6,6,", "6,7", "6,8", "6,9",
                "7,1", "7,2", "7,3", "7,4", "7,5", "7,6,", "7,7", "7,8", "7,9",
                "8,1", "8,2", "8,3", "8,4", "8,5", "8,6,", "8,7", "8,8", "8,9",
                "9,1", "9,2", "9,3", "9,4", "9,5", "9,6,", "9,7", "9,8", "9,9",
        };

        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.addAll(Arrays.asList(coords));

        return arrayList;
    }

    public static void log(String name) {
        Log.d("MY_LOG", name);
    }

    public static void log(String name, Object o) {

        if (o != null) {
            Log.d("MY_LOG", name + " = " + o.toString());
        } else {
            Log.d("MY_LOG", name + " = NULL");
        }
    }

    public static String buildTimeString(Context context, long time, boolean addLeadingZeroes) {
        long elapsedTime = time / 1000;
        long minutes = elapsedTime / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        String strSeconds;
        String strMinutes;
        String strHours;
        String strDays;

        if (addLeadingZeroes) {
            strSeconds = Utils.addLeadingZeroes(String.valueOf(elapsedTime % 60), 2);
            strMinutes = Utils.addLeadingZeroes(String.valueOf(minutes % 60), 2);
            strHours = Utils.addLeadingZeroes(String.valueOf(hours % 24), 2);
            strDays = Utils.addLeadingZeroes(String.valueOf(days), 3);
        } else {
            strSeconds = String.valueOf(elapsedTime % 60);
            strMinutes = String.valueOf(minutes % 60);
            strHours = String.valueOf(hours % 24);
            strDays = String.valueOf(days);
        }

        return String.format(
                context.getString(R.string.time_format),
                strDays,
                strHours,
                strMinutes,
                strSeconds
        );
    }

    public static String addLeadingZeroes(String s, int numZeroes) {
        StringBuilder stringBuilder = new StringBuilder(s);

        for (int i = 0; i < (numZeroes - s.length()); ++i) {
            stringBuilder = stringBuilder.insert(0, "0");
        }

        return stringBuilder.toString();
    }
}
