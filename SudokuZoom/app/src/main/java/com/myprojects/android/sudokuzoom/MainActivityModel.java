package com.myprojects.android.sudokuzoom;

import android.arch.lifecycle.ViewModel;

/**
 * Created by Owner on 2/15/2018.
 */

public class MainActivityModel extends ViewModel {

    private PuzzleData[] mPuzzleDataArray;
    private int mCurrentPuzzleId;

    private GameTimer mNewTimer;
    private GameTimer mCurrTimer;

    public PuzzleData[] getPuzzleDataArray() {
        return mPuzzleDataArray;
    }

    public void setPuzzleDataArray(PuzzleData[] puzzleDataArray) {
        mPuzzleDataArray = puzzleDataArray;
    }

    public int getCurrentPuzzleId() {
        return mCurrentPuzzleId;
    }

    public void setCurrentPuzzleId(int currentPuzzleId) {
        mCurrentPuzzleId = currentPuzzleId;
    }

    public GameTimer getNewTimer() {
        return mNewTimer;
    }

    public void setNewTimer(GameTimer gameTimer) {
        mNewTimer = gameTimer;
    }

    public GameTimer getCurrTimer() {
        return mCurrTimer;
    }

    public void setCurrTimer(GameTimer gameTimer) {
        mCurrTimer = gameTimer;
    }
}
