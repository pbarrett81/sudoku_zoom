package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextPaint;
import android.util.AttributeSet;

public class AutoSizingTextView extends AppCompatTextView {

    public static final String LOG_TAG = AutoSizingTextView.class.getSimpleName();

    private float minTextSize;
    private float maxTextSize;

    private TextPaint mTextPaint;

    public AutoSizingTextView(Context context) {
        super(context);
        init();
    }

    public AutoSizingTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        maxTextSize = this.getTextSize();

        if (maxTextSize < 35) {
            maxTextSize = 30;
        }

        minTextSize = 20;

        mTextPaint = this.getPaint();
    }
}