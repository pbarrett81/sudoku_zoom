/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.myprojects.android.sudokuzoom.LocalContract.GameInfoEntry;
import com.myprojects.android.sudokuzoom.LocalContract.TimedGamePuzzleInfoEntry;
import com.myprojects.android.sudokuzoom.LocalContract.UntimedGamePuzzleInfoEntry;
import com.myprojects.android.sudokuzoom.LocalContract.BestTimesEntry;

/**
 * Manages a local database for weather data.
 */
public class LocalDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 2;

    static final String DATABASE_NAME = "sudoku_zoom_local.db";

    public LocalDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        final String SQL_CREATE_GAME_INFO_TABLE = "CREATE TABLE IF NOT EXISTS " +
                GameInfoEntry.TABLE_NAME + " (" +
                GameInfoEntry.COLUMN_START_TIME + " VARCHAR(8) PRIMARY KEY," +
                GameInfoEntry.COLUMN_TOP_PUZZLE_ID + " VARCHAR(5)," +
                GameInfoEntry.COLUMN_CURRENT_PUZZLE_ID + " VARCHAR(5) " +
                " );";

        final String SQL_CREATE_TIMED_GAME_PUZZLE_INFO_TABLE = "CREATE TABLE IF NOT EXISTS " +
                TimedGamePuzzleInfoEntry.TABLE_NAME + " (" +
                TimedGamePuzzleInfoEntry.COLUMN_PUZZLE_ID + " VARCHAR(5) PRIMARY KEY," +
                TimedGamePuzzleInfoEntry.COLUMN_CHILD_PUZZLE_IDS + " VARCHAR(45), " +
                TimedGamePuzzleInfoEntry.COLUMN_SOLUTION + " VARCHAR(81), " +
                TimedGamePuzzleInfoEntry.COLUMN_GIVENS + " VARCHAR(81), " +
                TimedGamePuzzleInfoEntry.COLUMN_ENTERED + " VARCHAR(81), " +
                TimedGamePuzzleInfoEntry.COLUMN_LAYER + " INT, " +
                TimedGamePuzzleInfoEntry.COLUMN_SOLVED + " INT " +
                " );";

        final String SQL_CREATE_UNTIMED_GAME_PUZZLE_INFO_TABLE = "CREATE TABLE IF NOT EXISTS " +
                UntimedGamePuzzleInfoEntry.TABLE_NAME + " (" +
                UntimedGamePuzzleInfoEntry.COLUMN_PUZZLE_ID + " VARCHAR(5) PRIMARY KEY," +
                UntimedGamePuzzleInfoEntry.COLUMN_CHILD_PUZZLE_IDS + " VARCHAR(45), " +
                UntimedGamePuzzleInfoEntry.COLUMN_SOLUTION + " VARCHAR(81), " +
                UntimedGamePuzzleInfoEntry.COLUMN_GIVENS + " VARCHAR(81), " +
                UntimedGamePuzzleInfoEntry.COLUMN_ENTERED + " VARCHAR(81), " +
                UntimedGamePuzzleInfoEntry.COLUMN_LAYER + " INT, " +
                UntimedGamePuzzleInfoEntry.COLUMN_SOLVED + " INT " +
                " );";

        final String SQL_CREATE_BEST_TIMES_TABLE = "CREATE TABLE IF NOT EXISTS " +
                BestTimesEntry.TABLE_NAME + " (" +
                BestTimesEntry.COLUMN_LAYER_COUNT + " VARCHAR(1) PRIMARY KEY," +
                BestTimesEntry.COLUMN_COMPLETION_TIME + " VARCHAR(8), " +
                BestTimesEntry.COLUMN_DATE + " VARCHAR(6), " +
                BestTimesEntry.COLUMN_PLAYER_NAME + " VARCHAR(30) " +
                " );";

        sqLiteDatabase.execSQL(SQL_CREATE_GAME_INFO_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_TIMED_GAME_PUZZLE_INFO_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_UNTIMED_GAME_PUZZLE_INFO_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_BEST_TIMES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        onCreate(sqLiteDatabase);
    }
}
