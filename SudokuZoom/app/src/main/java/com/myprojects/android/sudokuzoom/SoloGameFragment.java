package com.myprojects.android.sudokuzoom;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Owner on 5/26/2017.
 */

public class SoloGameFragment extends Fragment {

    private RelativeLayout mRootLayout;
    private LinearLayout mProgressBarLayout;
    private ProgressBar mProgressBar;
    private TextView mProgressTextView1;
    private TextView mProgressTextView2;
    private LinearLayout mSavingGameLayout;
    private FrameLayout mGameContainer;
    private GameLayout mGameLayout;

    private int mTotalGeneratePuzzles = 0;
    private int mTotalLoadPuzzles = 0;

    private boolean mIsCreated = false;

    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mLocalBroadcastManager;

    private String mCurrentAction;

    private int mRootDimen = 0;

    private int mScreenOrientation = -1;

    private TextView mVersionNameTextView;

    public interface Callback {
        void onCancelButtonClicked();
        void onSoloGameFragmentDisplayed();
    }

    public void setCurrentAction(String currentAction) {
        mCurrentAction = currentAction;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_solo_game, container, false);

        mProgressBarLayout = (LinearLayout) rootView.findViewById(R.id.progress_bar_layout);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        mProgressTextView1 = (TextView) rootView.findViewById(R.id.progress_textview_1);
        mProgressTextView2 = (TextView) rootView.findViewById(R.id.progress_textview_2);

        mSavingGameLayout = (LinearLayout) rootView.findViewById(R.id.saving_game_layout);

        mGameContainer = (FrameLayout) rootView.findViewById(R.id.game_container);

        mScreenOrientation = this.getResources().getConfiguration().orientation;

        mRootLayout = (RelativeLayout) rootView.findViewById(R.id.root_layout);
        mRootLayout.getViewTreeObserver().addOnGlobalLayoutListener(

                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        mRootLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                        AppCompatActivity activity = (AppCompatActivity) mRootLayout.getContext();

                        if (mScreenOrientation == Configuration.ORIENTATION_PORTRAIT) {
                            mRootDimen = mRootLayout.getWidth();
                            mRootLayout.setBackgroundResource(R.drawable.background_main_portrait);
                        } else if (mScreenOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                            mRootDimen = mRootLayout.getHeight();
                            mRootLayout.setBackgroundResource(R.drawable.background_main_landscape);
                        }

                        ((Callback) activity).onSoloGameFragmentDisplayed();
                    }
                }
        );

        Bundle args = this.getArguments();

        if (args != null) {
            mCurrentAction = args.getString("ACTION");
            setDisplay();
        }

        initBroadcast();

        mVersionNameTextView = (TextView) rootView.findViewById(R.id.textview_version_name);

        final AppCompatActivity activity = (AppCompatActivity) getActivity();

        if (activity != null) {
            final Button cancelButton = rootView.findViewById(R.id.cancel_button);
            cancelButton.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    toggleProgressBarVisibility(View.INVISIBLE);
                    toggleSavingGameVisibility(View.INVISIBLE);
                    ((Callback) activity).onCancelButtonClicked();
                }
            });

            try {
                mVersionNameTextView.setText(
                        String.format(
                                activity.getString(R.string.version_name_format),
                                activity.getPackageManager().getPackageInfo(
                                        activity.getPackageName(),
                                        0
                                ).versionName
                        )
                );
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            mIsCreated = true;
        }

        return rootView;
    }

    public boolean isCreated() {
        return mIsCreated;
    }

    public void setDisplay() {

        if (mCurrentAction != null) {

            switch (mCurrentAction) {

                case MainActivity.ACTION_GENERATE_GAME:
                    notifyGenerateLayer(GenerateGameService.mLayerNum, GenerateGameService.mTotalPuzzles);
                    toggleProgressBarVisibility(View.VISIBLE);
                    toggleSavingGameVisibility(View.VISIBLE);
                    break;

                case MainActivity.ACTION_LOAD_GAME:
                    mProgressTextView1.setText(R.string.textview_loading);
                    notifyLoadTotal(DatabaseService.mLoadTotal);
                    toggleProgressBarVisibility(View.VISIBLE);
                    toggleSavingGameVisibility(View.INVISIBLE);
                    break;

                case MainActivity.ACTION_NONE:
                    toggleProgressBarVisibility(View.INVISIBLE);
                    toggleSavingGameVisibility(View.INVISIBLE);
                    break;
            }
        }
    }

    public void initBroadcast() {
        mBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                if (action != null) {

                    switch (action) {

                        case MainActivity.NOTIFY_GENERATE_LAYER:
                            notifyGenerateLayer(GenerateGameService.mLayerNum, GenerateGameService.mTotalPuzzles);
                            break;

                        case MainActivity.UPDATE_GENERATE_PROGRESS:
                            updateGenerateProgress(GenerateGameService.mPuzzleCount);
                            break;

                        case MainActivity.SAVING_GAME:
                            toggleProgressBarVisibility(View.INVISIBLE);
                            toggleSavingGameVisibility(View.VISIBLE);
                            break;

                        case MainActivity.NOTIFY_LOAD_TOTAL:
                            notifyLoadTotal(DatabaseService.mLoadTotal);
                            break;

                        case MainActivity.UPDATE_LOAD_PROGRESS:
                            updateLoadProgress(DatabaseService.mLoadCount);
                            break;
                    }
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MainActivity.NOTIFY_GENERATE_LAYER);
        intentFilter.addAction(MainActivity.UPDATE_GENERATE_PROGRESS);
        intentFilter.addAction(MainActivity.SAVING_GAME);
        intentFilter.addAction(MainActivity.NOTIFY_LOAD_TOTAL);
        intentFilter.addAction(MainActivity.UPDATE_LOAD_PROGRESS);

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this.getContext());
        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
    }

    public void initGameLayout(final GameTimer gameTimer, final boolean newGame) {

        if (mScreenOrientation == Configuration.ORIENTATION_PORTRAIT) {
            mGameLayout = (GameLayout) LayoutInflater.from(this.getContext()).inflate(
                    R.layout.layout_game_portrait,
                    null
            );
        } else if (mScreenOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            mGameLayout = (GameLayout) LayoutInflater.from(this.getContext()).inflate(
                    R.layout.layout_game_landscape,
                    null
            );
        }

        mGameLayout.setGameTimer(gameTimer);
        mGameLayout.buildDisplay(mRootDimen);

        mGameLayout.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        mGameLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                        if (newGame) {
                            gameTimer.setElapsedTime(0);
                            gameTimer.readyInitialDisplay();
                        }
                    }
                });

        displayGameLayout();
    }

    public void notifyGenerateLayer(int layerNum, int totalPuzzles) {
        mTotalGeneratePuzzles = totalPuzzles;
        mProgressBar.setMax(mTotalGeneratePuzzles);
        mProgressTextView1.setText(String.format(this.getString(R.string.progress_1), layerNum));
        updateGenerateProgress(GenerateGameService.mPuzzleCount);
    }

    public void updateGenerateProgress(int progress) {
        mProgressBar.setProgress(progress);
        mProgressTextView2.setText(String.format(
                this.getString(R.string.puzzle_generate_count),
                progress,
                mTotalGeneratePuzzles
        ));
    }

    public void notifyLoadTotal(int totalPuzzles) {
        mTotalLoadPuzzles = totalPuzzles;
        mProgressBar.setMax(mTotalLoadPuzzles);
        updateLoadProgress(DatabaseService.mLoadCount);
    }

    public void updateLoadProgress(int progress) {
        mProgressBar.setProgress(progress);
        mProgressTextView2.setText(String.format(
                this.getString(R.string.puzzle_load_count),
                progress,
                mTotalLoadPuzzles
        ));
    }

    public void displayGameLayout() {
        mGameContainer.removeAllViews();
        mGameContainer.addView(mGameLayout);

        if (mCurrentAction != null) {

            if (mCurrentAction.equals(MainActivity.ACTION_NONE)) {
                mProgressBarLayout.setVisibility(View.INVISIBLE);
            }
        }
    }

    public GameLayout getGameLayout() {
        return mGameLayout;
    }

    public void toggleProgressBarVisibility(int visibility) {
        mProgressBarLayout.setVisibility(visibility);
    }

    public void toggleSavingGameVisibility(int visibility) {
        mSavingGameLayout.setVisibility(visibility);
    }

    public void dismissCellPopup() {

        if (mGameLayout != null) {
            mGameLayout.dismissCellPopup();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocalBroadcastManager.unregisterReceiver(mBroadcastReceiver);

        if (mGameLayout != null) {
            mGameLayout.destroy();
        }
    }
}
