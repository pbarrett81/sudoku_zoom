package com.myprojects.android.sudokuzoom;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Owner on 10/26/2017.
 */

public class CellTranslateDecoration extends RecyclerView.ItemDecoration {

    private int mShift;

    public CellTranslateDecoration(int shift) {
        this.mShift = shift;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        PuzzleViewAdapter parentAdapter = (PuzzleViewAdapter) parent.getAdapter();

        if (Utils.searchIntArray(parentAdapter.findCurrentSection(), parent.getChildAdapterPosition(view))) {
            outRect.left = mShift;
            outRect.right = -mShift;
        }
    }
}
