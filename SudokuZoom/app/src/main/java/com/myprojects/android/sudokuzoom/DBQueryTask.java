package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;

/**
 * Created by Owner on 1/4/2018.
 */

public class DBQueryTask extends AsyncTask<DBQueryTask.Data, Integer, Cursor> {

    public static final String LOG_TAG = DBQueryTask.class.getSimpleName();

    public static class Data {

        private WeakReference<Context> mContextRef;
        private Uri mUri;
        private String[] mProjection;
        private String mSelection;
        private String[] mSelectionArgs;
        private String mSortOrder;

        public Data(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
            mContextRef = new WeakReference<>(context);
            mUri = uri;
            mProjection = projection;
            mSelection = selection;
            mSelectionArgs = selectionArgs;
            mSortOrder = sortOrder;
        }
    }

    @Override
    protected Cursor doInBackground(Data... params) {
        Data data = params[0];
        Context context = data.mContextRef.get();

        if (context != null) {
            return context.getContentResolver().query(
                    data.mUri,
                    data.mProjection,
                    data.mSelection,
                    data.mSelectionArgs,
                    data.mSortOrder
            );
            /*LocalDbHelper localDbHelper = new LocalDbHelper(context);
            return localDbHelper.getReadableDatabase().query(
                    data.mTable,
                    data.mProjection,
                    data.mSelection,
                    data.mSelectionArgs,
                    null,
                    null,
                    data.mSortOrder
            );*/
        } else {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Cursor result) {
        super.onPostExecute(result);
    }
}
