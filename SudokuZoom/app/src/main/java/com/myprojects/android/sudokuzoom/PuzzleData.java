package com.myprojects.android.sudokuzoom;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;

/**
 * Created by Owner on 1/30/2018.
 */

public class PuzzleData implements Parcelable {

    private CellData[] mCellDataArray = new CellData[81];

    private int mPuzzleId;
    private int[] mChildPuzzleIds = {-1, -1, -1, -1, -1, -1, -1, -1, -1};
    private int mLayerNum;

    public static final Parcelable.Creator<PuzzleData> CREATOR
            = new Parcelable.Creator<PuzzleData>() {

        public PuzzleData createFromParcel(Parcel in) {
            return new PuzzleData(in);
        }

        public PuzzleData[] newArray(int size) {
            return new PuzzleData[size];
        }
    };

    public PuzzleData(int layerNum, int puzzleId) {
        mLayerNum = layerNum;
        mPuzzleId = puzzleId;

        for (int i = 0; i < mCellDataArray.length; ++i) {
            mCellDataArray[i] = new CellData();
        }
    }

    private PuzzleData(Parcel in) {
        in.readTypedArray(mCellDataArray, CellData.CREATOR);
        mPuzzleId = in.readInt();
        in.readIntArray(mChildPuzzleIds);
        mLayerNum = in.readInt();
    }

    public CellData[] getCellDataArray() {
        return mCellDataArray;
    }

    public int getPuzzleId() {
        return mPuzzleId;
    }

    public int getChildPuzzleId(int section) {
        return mChildPuzzleIds[section];
    }

    public void setChildPuzzleId(int id, int section) {
        mChildPuzzleIds[section] = id;
    }

    public int getLayerNum() {
        return mLayerNum;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeTypedArray(mCellDataArray, flags);
        out.writeInt(mPuzzleId);
        out.writeIntArray(mChildPuzzleIds);
        out.writeInt(mLayerNum);
    }

    public void setGivensToSolved(int section) {

        for (int i = 0; i < mCellDataArray.length; ++i) {
            CellData cellData = mCellDataArray[i];

            if (cellData.getGiven()) {

                if (section != -1) {

                    if (Utils.searchIntArray(SudokuConstants.ADAPTER_SECTIONS[section], i)) {
                        cellData.setEnteredToAnswer();
                        cellData.setSolved(true);
                    }
                } else {
                    cellData.setEnteredToAnswer();
                    cellData.setSolved(true);
                }
            }
        }
    }

    public void setSudoku(int[][] sudoku) {
        int rowCount = 0;
        int colCount = 0;

        for (CellData cellData : mCellDataArray) {

            if (sudoku[rowCount][colCount] > 0) {
                cellData.setGiven(true);
            } else {
                cellData.setGiven(false);
            }

            if (colCount == 8) {
                rowCount++;
                colCount = 0;
            } else {
                colCount++;
            }
        }
    }

    public void setAnswer(int[][] answer) {
        int rowCount = 0;
        int colCount = 0;

        for (CellData cellData : mCellDataArray) {
            cellData.setAnswer(answer[rowCount][colCount]);

            if (colCount == 8) {
                rowCount++;
                colCount = 0;
            } else {
                colCount++;
            }
        }
    }

    public boolean isSectionEmpty(int section) {
        int[] positions = SudokuConstants.ADAPTER_SECTIONS[section];

        for (int i : positions) {

            if (mCellDataArray[i].getGiven()) {
                return false;
            }
        }

        return true;
    }

    public String buildChildPuzzleIdString() {
        String childPuzzleIds = "";

        for (int i = 0; i < mChildPuzzleIds.length; ++i) {
            String id = "";

            if (mChildPuzzleIds[i] == -1) {
                id = "00000";
            } else {
                id = String.valueOf(mChildPuzzleIds[i]);
                String zeroes = "";

                for (int j = 0; j < (5 - id.length()); ++j) {
                    zeroes = zeroes.concat("0");
                }

                id = zeroes.concat(id);
            }

            childPuzzleIds = childPuzzleIds.concat(id);
        }

        return childPuzzleIds;
    }

    public String buildSolutionString() {
        String solution = "";

        for (CellData cellData : mCellDataArray) {
            solution = solution.concat(String.valueOf(cellData.getAnswer()));
        }

        return solution;
    }

    public String buildGivenString() {
        String givens = "";

        for (CellData cellData : mCellDataArray) {

            if (cellData.getGiven()) {
                givens = givens.concat("1");
            } else {
                givens = givens.concat("0");
            }
        }

        return givens;
    }

    public void parseChildPuzzleIds(String childIds) {
        int idCount = 0;

        for (int i = 0; i < 9; ++i) {
            int id = Integer.valueOf(childIds.substring(idCount, idCount + 5));

            if (id > 0) {
                setChildPuzzleId(id, i);
            }

            idCount += 5;
        }
    }

    public void parseSolution(String solution) {

        for (int i = 0; i < solution.length(); ++i) {
            mCellDataArray[i].setAnswer(Character.getNumericValue(solution.charAt(i)));
        }
    }

    public void parseGivens(String givens) {

        for (int i = 0; i < mCellDataArray.length; ++i) {
            int value = Character.getNumericValue(givens.charAt(i));

            if (value > 0) {
                mCellDataArray[i].setGiven(true);
            } else {
                mCellDataArray[i].setGiven(false);
            }
        }
    }

    public void parseEntered(String entered) {

        for (int i = 0; i < mCellDataArray.length; ++i) {
            mCellDataArray[i].setEntered(Character.getNumericValue(entered.charAt(i)));
        }
    }

    public String buildEnteredString() {
        String entered = "";

        for (CellData cellData : mCellDataArray) {
            entered = entered.concat(String.valueOf(cellData.getEntered()));
        }

        return entered;
    }

    public int findSection(CellLayout cellLayout) {
        int position = Arrays.asList(mCellDataArray).indexOf(cellLayout.getCellData());

        for (int i = 0; i < 9; ++i) {

            if (Utils.searchIntArray(SudokuConstants.ADAPTER_SECTIONS[i], position)) {
                return i;
            }
        }

        return -1;
    }

    public boolean checkAnswer() {
        boolean correct = true;

        for (CellData cellData : mCellDataArray) {

            if (!cellData.checkAnswer()) {
                correct = false;
            }
        }

        if (correct) {
            setAllToSolved();
        }

        return correct;
    }

    public void setAllToSolved() {

        for (CellData cellData : mCellDataArray) {
            cellData.setSolved(true);
        }
    }

    public boolean checkSolved() {

        for (CellData cellData : mCellDataArray) {

            if (!cellData.getSolved()) {
                return false;
            }
        }

        return true;
    }

    public boolean findChildAndSetToSolved(int childPuzzleId) {
        int childSection = findChildSection(childPuzzleId);

        if (childSection != -1) {
            setGivensToSolved(childSection);
            return true;
        }

        return false;
    }

    public int findChildSection(int childPuzzleId) {
        int section = -1;

        for (int i = 0; i < mChildPuzzleIds.length; ++i) {

            if (mChildPuzzleIds[i] == childPuzzleId) {
                section = i;
            }
        }

        return section;
    }
}