package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by Owner on 9/7/2017.
 */

public class PuzzleLayout extends FrameLayout {

    private Context mContext;

    private PuzzleView mPuzzleView;
    private SectionView mSectionView;

    private int mTotalBorderWidth;
    private float mScaledBorderWidth;

    private TextView mCheckmark;

    private boolean mComplete = false;

    public PuzzleLayout(Context context) {
        super(context);
        mContext = context;

        mPuzzleView = new PuzzleView(mContext);
        mSectionView = new SectionView(mContext);

        mCheckmark = new TextView(mContext);
        mCheckmark.setTypeface(null, Typeface.BOLD);
        mCheckmark.setTextColor(Color.parseColor("#8000FF00"));
        mCheckmark.setText(Html.fromHtml(mContext.getString(R.string.checkmark)));
        mCheckmark.setIncludeFontPadding(false);
        mCheckmark.setGravity(Gravity.CENTER);
    }

    public float getScaledBorderWidth() {
        return mScaledBorderWidth;
    }

    public void setScaledBorderWidth(float borderWidth) {
        mScaledBorderWidth = borderWidth;
    }

    public PuzzleView getPuzzleView() {
        return mPuzzleView;
    }

    public boolean getComplete() {
        return mComplete;
    }

    public void initDisplay(int parentDimen, float parentBorderWidth, float floatTotalBorderWidth) {
        int cWidth = (int) Math.floor((parentDimen - (2 * parentBorderWidth)) / 9);
        int layoutHeight = cWidth * 9;
        mTotalBorderWidth = (int) Math.ceil(floatTotalBorderWidth);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(layoutHeight, layoutHeight);
        layoutParams.topMargin = mTotalBorderWidth;
        layoutParams.leftMargin = mTotalBorderWidth;
        setLayoutParams(layoutParams);

        int textHeight = layoutHeight;

        if (textHeight > 575) {
            textHeight = 575;
        }

        mCheckmark.setTextSize(TypedValue.COMPLEX_UNIT_PX, textHeight);

        this.addView(mPuzzleView);
        this.addView(mSectionView);
    }

    public void showComplete() {

        if (!mComplete) {
            addView(mCheckmark);
            mComplete = true;
        }
    }

    public void setSectionColors() {
        int layerNum = ((PuzzleViewAdapter) mPuzzleView.getAdapter()).getPuzzleData().getLayerNum();
        int color = -1;

        switch (layerNum) {

            case 1:
                color = R.color.colorSection1;
                break;

            case 2:
                color = R.color.colorSection2;
                break;

            case 3:
                color = R.color.colorSection3;
                break;
        }

        ((SectionViewAdapter) mSectionView.getAdapter()).setSectionColor(color);
    }
}
