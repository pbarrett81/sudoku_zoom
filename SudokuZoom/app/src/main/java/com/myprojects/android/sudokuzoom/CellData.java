package com.myprojects.android.sudokuzoom;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Owner on 1/28/2018.
 */

public class CellData implements Parcelable {

    private int mAnswer;
    private int mEntered = 0;

    private boolean mGiven;
    private boolean mSolved = false;

    public static final Parcelable.Creator<CellData> CREATOR
            = new Parcelable.Creator<CellData>() {

        public CellData createFromParcel(Parcel in) {
            return new CellData(in);
        }

        public CellData[] newArray(int size) {
            return new CellData[size];
        }
    };

    private CellData(Parcel in) {
        mAnswer = in.readInt();
        mEntered = in.readInt();
        mGiven = in.readInt() > 0;
        mSolved = in.readInt() > 0;
    }

    public CellData() {}

    public void setAnswer(int answer) {
        mAnswer = answer;
    }

    public int getAnswer() {
        return mAnswer;
    }

    public boolean getGiven() {
        return mGiven;
    }

    public void setGiven(boolean b) {
        mGiven = b;
    }

    public int getEntered() {
        return mEntered;
    }

    public void setEntered(int entered) {
        mEntered = entered;
    }

    public boolean getSolved() {
        return mSolved;
    }

    public void setSolved(boolean solved) {
        mSolved = solved;
    }

    public boolean checkAnswer() {
        //Utils.log("mEntered", mEntered);
        //Utils.log("mAnswer", mAnswer);
        return mEntered == mAnswer;
    }

    public void setEnteredToAnswer() {
        mEntered = mAnswer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(mAnswer);
        out.writeInt(mEntered);
        out.writeInt(mGiven ? 1 : 0);
        out.writeInt(mSolved ? 1 : 0);
    }
}
