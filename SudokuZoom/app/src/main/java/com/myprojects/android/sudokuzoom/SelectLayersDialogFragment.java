package com.myprojects.android.sudokuzoom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;

/**
 * Created by Owner on 11/19/2017.
 */

public class SelectLayersDialogFragment extends DialogFragment {

    private Context mContext;

    private AlertDialog mAlertDialog;

    public interface Callback {
        void onLayersOkSelected(int numLayers);
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.textview_number_of_layers);

        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_select_layers, null);
        final RadioGroup radioGroup = (RadioGroup) contentView.findViewById(R.id.rg);
        builder.setView(contentView);

        builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                int selected = radioGroup.getCheckedRadioButtonId();

                if (selected != -1) {
                    int numLayers = 0;

                    switch (selected) {

                        case R.id.radio_1:
                            numLayers = 1;
                            break;

                        case R.id.radio_2:
                            numLayers = 2;
                            break;

                        case R.id.radio_3:
                            numLayers = 3;
                            break;

                    }

                    ((SelectLayersDialogFragment.Callback) getActivity()).onLayersOkSelected(numLayers);
                }
            }
        });

        builder.setNegativeButton(R.string.cancel, null);

        mAlertDialog = builder.create();

        return mAlertDialog;
    }
}
