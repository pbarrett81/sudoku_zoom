package com.myprojects.android.sudokuzoom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Date;
import java.util.concurrent.ExecutionException;

/**
 * Created by Owner on 11/19/2017.
 */

public class BestTimesDialogFragment extends DialogFragment {

    private Context mContext;

    private AlertDialog mAlertDialog;

    private TextView mLayersHeaderTextView;
    private TextView mTimesHeaderTextView;
    private TextView mTimesTextView1;
    private TextView mTimesTextView2;
    private TextView mTimesTextView3;

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.title_best_times);

        ScrollView contentView = (ScrollView) LayoutInflater.from(mContext).inflate(R.layout.layout_best_times, null);

        mLayersHeaderTextView = (TextView) contentView.findViewById(R.id.layers_header_textview);
        mTimesHeaderTextView = (TextView) contentView.findViewById(R.id.times_header_textview);
        mTimesTextView1 = (TextView) contentView.findViewById(R.id.times_textview_1);
        mTimesTextView2 = (TextView) contentView.findViewById(R.id.times_textview_2);
        mTimesTextView3 = (TextView) contentView.findViewById(R.id.times_textview_3);

        mTimesTextView1.setText(R.string.not_applicable);
        mTimesTextView2.setText(R.string.not_applicable);
        mTimesTextView3.setText(R.string.not_applicable);

        DBQueryTask dbQueryTask = new DBQueryTask();
        dbQueryTask.execute(
                new DBQueryTask.Data(
                        mContext,
                        LocalContract.BestTimesEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        null
                )
        );

        Cursor cursor = null;

        try {
            cursor = dbQueryTask.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (cursor != null) {

            while (cursor.moveToNext()) {
                int numLayers = Integer.valueOf(cursor.getString(cursor.getColumnIndex(
                        LocalContract.BestTimesEntry.COLUMN_LAYER_COUNT
                )));
                String completionTime = Utils.buildTimeString(
                        this.getContext(),
                        Long.parseLong(
                                cursor.getString(
                                        cursor.getColumnIndex(
                                                LocalContract.BestTimesEntry.COLUMN_COMPLETION_TIME
                                        )
                                )
                        ),
                        false
                );
                String date = formatDate(Long.parseLong(cursor.getString(cursor.getColumnIndex(
                        LocalContract.BestTimesEntry.COLUMN_DATE
                ))));
                String playerName = cursor.getString(cursor.getColumnIndex(
                        LocalContract.BestTimesEntry.COLUMN_PLAYER_NAME
                ));
                String record = String.format(
                        this.getString(R.string.best_time_format),
                        completionTime,
                        date,
                        playerName
                );

                switch (numLayers) {

                    case 1:
                        mTimesTextView1.setText(record);
                        break;

                    case 2:
                        mTimesTextView2.setText(record);
                        break;

                    case 3:
                        mTimesTextView3.setText(record);
                        break;
                }
            }

            cursor.close();
        }

        builder.setView(contentView);

        builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dismiss();
            }
        });

        mAlertDialog = builder.create();

        return mAlertDialog;
    }

    public String formatDate(long time) {
        Date date = new Date(time);
        String[] timeUnits = date.toString().split(" ");
        return String.format(
                        this.getString(R.string.textview_new_best_time_4),
                        timeUnits[1],
                        timeUnits[2],
                        timeUnits[5]
                );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
