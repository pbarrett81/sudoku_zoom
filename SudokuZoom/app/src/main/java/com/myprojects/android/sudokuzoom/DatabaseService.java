package com.myprojects.android.sudokuzoom;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;

/**
 * Created by Owner on 1/27/2018.
 */

public class DatabaseService extends JobIntentService {

    private static final int JOB_ID = 1001;

    public static boolean running = false;
    public static int mLoadTotal = 0;
    public static int mLoadCount = 0;

    private BroadcastReceiver mBroadcastReceiver;
    private boolean mStopWork = false;

    /**
     * Convenience method for enqueuing work in to this service.
     */
    static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, DatabaseService.class, JOB_ID, work);
    }

    public DatabaseService() {
        super();

        resetProgress();

        mBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(MainActivity.STOP_WORK)) {
                    mStopWork = true;
                    //Utils.log("STOP_WORK");
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MainActivity.STOP_WORK);
        intentFilter.addAction(MainActivity.CONFIG_CHANGE);

        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, intentFilter);
    }

    @Override
    public void onHandleWork (@NonNull Intent intent) {
        running = true;

        String action = intent.getAction();

        if (action != null) {

            switch (action) {

                case MainActivity.ACTION_LOAD_GAME:
                    loadGame(intent);
                    break;

                case MainActivity.ACTION_SAVE_GAME_PUZZLE_INFO:
                    saveGamePuzzleInfo(intent);
                    break;

                case MainActivity.ACTION_SAVE_GAME_INFO:
                    saveGameInfo(intent);
                    break;

                case MainActivity.ACTION_DB_INSERT:
                    doInsert(intent);
                    break;

                case MainActivity.ACTION_DB_DELETE:
                    doDelete(intent);
                    break;

                case MainActivity.ACTION_DB_UPDATE:
                    doUpdate(intent);
                    break;
            }
        }

        running = false;
        resetProgress();
    }

    public void loadGame(Intent intent) {
        boolean timed = intent.getBooleanExtra(
                "com.myprojects.android.sudokuzoom.Timed",
                false);
        Cursor cursor;
        int loadCount = 0;

        ArrayList<PuzzleData> puzzleDataList = new ArrayList<>();

        stopWork:
        {
            if (timed) {
                cursor = this.getContentResolver().query(
                        LocalContract.TimedGamePuzzleInfoEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        null);

                if (cursor != null) {
                    notifyLoadTotal(cursor.getCount());

                    while (cursor.moveToNext()) {

                        if (mStopWork) {
                            break stopWork;
                        }

                        String puzzleId = cursor.getString(cursor.getColumnIndex(
                                LocalContract.TimedGamePuzzleInfoEntry.COLUMN_PUZZLE_ID)
                        );
                        String childPuzzleIds = cursor.getString(cursor.getColumnIndex(
                                LocalContract.TimedGamePuzzleInfoEntry.COLUMN_CHILD_PUZZLE_IDS)
                        );
                        String solution = cursor.getString(cursor.getColumnIndex(
                                LocalContract.TimedGamePuzzleInfoEntry.COLUMN_SOLUTION)
                        );
                        String givens = cursor.getString(cursor.getColumnIndex(
                                LocalContract.TimedGamePuzzleInfoEntry.COLUMN_GIVENS)
                        );
                        String entered = cursor.getString(cursor.getColumnIndex(
                                LocalContract.TimedGamePuzzleInfoEntry.COLUMN_ENTERED)
                        );
                        int layer = cursor.getInt(cursor.getColumnIndex(
                                LocalContract.TimedGamePuzzleInfoEntry.COLUMN_LAYER)
                        );
                        int solved = cursor.getInt(cursor.getColumnIndex(
                                LocalContract.TimedGamePuzzleInfoEntry.COLUMN_SOLVED)
                        );

                        PuzzleData puzzleData = new PuzzleData(layer, Integer.valueOf(puzzleId));
                        puzzleData.parseChildPuzzleIds(childPuzzleIds);
                        puzzleData.parseSolution(solution);
                        puzzleData.parseGivens(givens);
                        puzzleData.parseEntered(entered);

                        if (solved > 0) {
                            puzzleData.setAllToSolved();

                            int count = 0;
                            boolean foundChild = false;

                            while (!foundChild && count < puzzleDataList.size()) {
                                PuzzleData currPuzzleData = puzzleDataList.get(count);
                                foundChild = currPuzzleData.findChildAndSetToSolved(puzzleData.getPuzzleId());
                                count++;
                            }
                        }

                        puzzleDataList.add(puzzleData);
                        loadCount++;
                        updateLoadProgress(loadCount);
                    }

                    cursor.close();
                    setBottomLayerGivensToSolved(puzzleDataList);
                }
            } else {
                cursor = this.getContentResolver().query(
                        LocalContract.UntimedGamePuzzleInfoEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        null);

                if (cursor != null) {
                    notifyLoadTotal(cursor.getCount());

                    while (cursor.moveToNext()) {

                        if (mStopWork) {
                            break stopWork;
                        }

                        String puzzleId = cursor.getString(cursor.getColumnIndex(
                                LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_PUZZLE_ID)
                        );
                        String childPuzzleIds = cursor.getString(cursor.getColumnIndex(
                                LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_CHILD_PUZZLE_IDS)
                        );
                        String solution = cursor.getString(cursor.getColumnIndex(
                                LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_SOLUTION)
                        );
                        String givens = cursor.getString(cursor.getColumnIndex(
                                LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_GIVENS)
                        );
                        String entered = cursor.getString(cursor.getColumnIndex(
                                LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_ENTERED)
                        );
                        int layer = cursor.getInt(cursor.getColumnIndex(
                                LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_LAYER)
                        );
                        int solved = cursor.getInt(cursor.getColumnIndex(
                                LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_SOLVED)
                        );

                        PuzzleData puzzleData = new PuzzleData(layer, Integer.valueOf(puzzleId));
                        puzzleData.parseChildPuzzleIds(childPuzzleIds);
                        puzzleData.parseSolution(solution);
                        puzzleData.parseGivens(givens);
                        puzzleData.parseEntered(entered);

                        if (solved > 0) {
                            puzzleData.setAllToSolved();

                            int count = 0;
                            boolean foundChild = false;

                            while (!foundChild && count < puzzleDataList.size()) {
                                PuzzleData currPuzzleData = puzzleDataList.get(count);
                                foundChild = currPuzzleData.findChildAndSetToSolved(puzzleData.getPuzzleId());
                                count++;
                            }
                        }

                        puzzleDataList.add(puzzleData);
                        loadCount++;
                        updateLoadProgress(loadCount);
                    }

                    cursor.close();
                    setBottomLayerGivensToSolved(puzzleDataList);
                }
            }
        }

        Intent broadcastIntent = new Intent(MainActivity.LOAD_WORK_FINISHED);
        broadcastIntent.putExtra("com.myprojects.android.sudokuzoom.WorkStopped", mStopWork);
        broadcastIntent.putParcelableArrayListExtra(
                "com.myprojects.android.sudokuzoom.PuzzleDataList",
                puzzleDataList
        );
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);

        mStopWork = false;
    }

    public void saveGamePuzzleInfo(Intent intent) {
        boolean timed = intent.getBooleanExtra(
                "com.myprojects.android.sudokuzoom.Timed",
                false
        );
        ArrayList<Parcelable> parcelables = intent.getParcelableArrayListExtra(
                "com.myprojects.android.sudokuzoom.PuzzleDataList"
        );
        ArrayList<PuzzleData> puzzleDataList = new ArrayList<>();

        for (Parcelable parcelable : parcelables) {
            puzzleDataList.add((PuzzleData) parcelable);
        }

        ContentResolver contentResolver = this.getContentResolver();
        Cursor cursor;

        if (timed) {
            cursor = contentResolver.query(
                    LocalContract.TimedGamePuzzleInfoEntry.CONTENT_URI,
                    null,
                    null,
                    null,
                    null
            );

            if (cursor != null) {
                cursor.moveToFirst();

                if (cursor.getCount() > 0) {
                    contentResolver.delete(
                            LocalContract.TimedGamePuzzleInfoEntry.CONTENT_URI,
                            null,
                            null
                    );
                }

                cursor.close();
            }
        } else {
            cursor = contentResolver.query(
                    LocalContract.UntimedGamePuzzleInfoEntry.CONTENT_URI,
                    null,
                    null,
                    null,
                    null
            );

            if (cursor != null) {
                cursor.moveToFirst();

                if (cursor.getCount() > 0) {
                    contentResolver.delete(
                            LocalContract.UntimedGamePuzzleInfoEntry.CONTENT_URI,
                            null,
                            null
                    );
                }

                cursor.close();
            }
        }

        /*String entered = "";

        for (int i = 0; i < 81; ++i) {
            entered = entered.concat("0");
        }*/

        for (PuzzleData puzzleData : puzzleDataList) {
            ContentValues contentValues = new ContentValues();

            String puzzleId = String.valueOf(puzzleData.getPuzzleId());
            String childPuzzleIds = puzzleData.buildChildPuzzleIdString();
            String solution = puzzleData.buildSolutionString();
            String givens = puzzleData.buildGivenString();
            String entered = puzzleData.buildEnteredString();
            int layer = puzzleData.getLayerNum();

            if (timed) {
                contentValues.put(LocalContract.TimedGamePuzzleInfoEntry.COLUMN_PUZZLE_ID, puzzleId);
                contentValues.put(LocalContract.TimedGamePuzzleInfoEntry.COLUMN_CHILD_PUZZLE_IDS, childPuzzleIds);
                contentValues.put(LocalContract.TimedGamePuzzleInfoEntry.COLUMN_SOLUTION, solution);
                contentValues.put(LocalContract.TimedGamePuzzleInfoEntry.COLUMN_GIVENS, givens);
                contentValues.put(LocalContract.TimedGamePuzzleInfoEntry.COLUMN_ENTERED, entered);
                contentValues.put(LocalContract.TimedGamePuzzleInfoEntry.COLUMN_LAYER, layer);
                contentValues.put(LocalContract.TimedGamePuzzleInfoEntry.COLUMN_SOLVED, 0);

                // Finally, insert data into the database.
                contentResolver.insert(
                        LocalContract.TimedGamePuzzleInfoEntry.CONTENT_URI,
                        contentValues
                );
            } else {
                contentValues.put(LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_PUZZLE_ID, puzzleId);
                contentValues.put(LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_CHILD_PUZZLE_IDS, childPuzzleIds);
                contentValues.put(LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_SOLUTION, solution);
                contentValues.put(LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_GIVENS, givens);
                contentValues.put(LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_ENTERED, entered);
                contentValues.put(LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_LAYER, layer);
                contentValues.put(LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_SOLVED, 0);

                // Finally, insert data into the database.
                contentResolver.insert(
                        LocalContract.UntimedGamePuzzleInfoEntry.CONTENT_URI,
                        contentValues
                );
            }
        }

        Intent broadcastIntent = new Intent(MainActivity.SAVE_GAME_PUZZLE_INFO_FINISHED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    public void saveGameInfo(Intent intent) {
        long startTime = intent.getLongExtra("com.myprojects.android.sudokuzoom.StartTime", -1);
        int topPuzzleId = intent.getIntExtra("com.myprojects.android.sudokuzoom.TopPuzzleId", -1);

        if (startTime > 0) {
            this.getContentResolver().delete(
                    LocalContract.GameInfoEntry.CONTENT_URI,
                    LocalContract.GameInfoEntry.COLUMN_START_TIME + " > 0",
                    null
            );
        } else {
            this.getContentResolver().delete(
                    LocalContract.GameInfoEntry.CONTENT_URI,
                    LocalContract.GameInfoEntry.COLUMN_START_TIME + " = 0",
                    null
            );
        }

        ContentValues cv = new ContentValues();
        cv.put(
                LocalContract.GameInfoEntry.COLUMN_TOP_PUZZLE_ID,
                String.valueOf(topPuzzleId)
        );
        cv.put(
                LocalContract.GameInfoEntry.COLUMN_START_TIME,
                String.valueOf(startTime)
        );
        cv.put(
                LocalContract.GameInfoEntry.COLUMN_CURRENT_PUZZLE_ID,
                String.valueOf(topPuzzleId)
        );
        this.getContentResolver().insert(LocalContract.GameInfoEntry.CONTENT_URI, cv);

        Intent broadcastIntent = new Intent(MainActivity.SAVE_GAME_INFO_FINISHED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    public void doInsert(Intent intent) {
        Uri uri = intent.getParcelableExtra("com.myprojects.android.sudokuzoom.Uri");
        ContentValues contentValues = intent.getParcelableExtra("com.myprojects.android.sudokuzoom.ContentValues");
        this.getContentResolver().insert(uri, contentValues);
    }

    public void doDelete(Intent intent) {
        Uri uri = intent.getParcelableExtra("com.myprojects.android.sudokuzoom.Uri");

        String selection = intent.getStringExtra("com.myprojects.android.sudokuzoom.Selection");
        selection = checkSelection(selection);

        String[] selectionArgs = intent.getStringArrayExtra("com.myprojects.android.sudokuzoom.SelectionArgs");
        selectionArgs = checkSelectionArgs(selectionArgs);

        this.getContentResolver().delete(uri, selection, selectionArgs);
    }

    public void doUpdate(Intent intent) {
        Uri uri = intent.getParcelableExtra("com.myprojects.android.sudokuzoom.Uri");
        ContentValues contentValues = intent.getParcelableExtra("com.myprojects.android.sudokuzoom.ContentValues");

        String selection = intent.getStringExtra("com.myprojects.android.sudokuzoom.Selection");
        selection = checkSelection(selection);

        String[] selectionArgs = intent.getStringArrayExtra("com.myprojects.android.sudokuzoom.SelectionArgs");
        selectionArgs = checkSelectionArgs(selectionArgs);

        this.getContentResolver().update(uri, contentValues, selection, selectionArgs);
    }

    public void setBottomLayerGivensToSolved(ArrayList<PuzzleData> puzzleDataList) {
        int numLayers = puzzleDataList.get(puzzleDataList.size() - 1).getLayerNum();

        for (PuzzleData puzzleData : puzzleDataList) {

            if (puzzleData.getLayerNum() == numLayers) {
                puzzleData.setGivensToSolved(-1);
            }
        }
    }

    public String checkSelection(String selection) {

        if (selection != null) {

            if (selection.equals("")) {
                selection = null;
            }
        }

        return selection;
    }

    public String[] checkSelectionArgs(String[] selectionArgs) {

        if (selectionArgs.length == 0) {
            selectionArgs = null;
        }

        return selectionArgs;
    }

    public void notifyLoadTotal(int loadTotal) {
        Intent intent = new Intent(MainActivity.NOTIFY_LOAD_TOTAL);
        mLoadTotal = loadTotal;
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void updateLoadProgress(int loadCount) {
        Intent intent = new Intent(MainActivity.UPDATE_LOAD_PROGRESS);
        mLoadCount = loadCount;
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    }

    public void resetProgress() {
        mLoadTotal = 0;
        mLoadCount = 0;
    }
}