package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;

/**
 * Created by Owner on 9/20/2017.
 */

public class SectionLayout extends FrameLayout {

    public static final String LOG_TAG = SectionLayout.class.getSimpleName();

    private final int STANDARD_SECTION_WIDTH = 231;
    private Resources mResources;

    private int wStrokeWidth;
    private int tStrokeWidth;

    public SectionLayout(Context context) {
        super(context);
    }

    public SectionLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SectionLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void init() {
        mResources = this.getResources();

        wStrokeWidth = mResources.getDimensionPixelSize(R.dimen.puzzle_stroke_wide);
        tStrokeWidth = mResources.getDimensionPixelSize(R.dimen.puzzle_stroke_thin);
    }

    public float determineScale() {
        return (float) this.getWidth() / STANDARD_SECTION_WIDTH;
    }

    public void setStrokeProperties(float scale, int strokeColor) {
        tStrokeWidth = Math.round(mResources.getDimensionPixelSize(R.dimen.puzzle_stroke_thin) * scale);

        if (tStrokeWidth == 0) {
            tStrokeWidth = 1;
        }

        wStrokeWidth = Math.round(mResources.getDimensionPixelSize(R.dimen.puzzle_stroke_wide) * scale);

        if (wStrokeWidth == 0) {
            wStrokeWidth = 1;
        }

        LayerDrawable layerDrawable = (LayerDrawable) this.getBackground();

        ((GradientDrawable) layerDrawable.getDrawable(0)).setStroke(
                wStrokeWidth,
                mResources.getColor(R.color.colorCellStroke2)
        );

        ((GradientDrawable) layerDrawable.getDrawable(1)).setStroke(
                tStrokeWidth,
                mResources.getColor(strokeColor)
        );
    }

    public int calculateOpeningWidth() {
        return this.getWidth() - (2 * wStrokeWidth);
    }

    public void runScaleAnimation() {
        ScaleAnimation scaleAnim = new ScaleAnimation(
                1f,
                3f,
                1f,
                3f
        );
        scaleAnim.setDuration(1000);
        scaleAnim.setFillAfter(true);
        this.startAnimation(scaleAnim);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
