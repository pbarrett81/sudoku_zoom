package com.myprojects.android.sudokuzoom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;

/**
 * Created by Owner on 11/19/2017.
 */

public class NewBestTimeDialogFragment extends DialogFragment {

    private Context mContext;

    private AlertDialog mAlertDialog;

    private TextView mCongratsTextView;
    private TextView mNewTimeTextView;
    private TextView mNewDateTextView;
    private EditText mNewPlayerNameEditText;
    private TextView mPrevTimeTextView;
    private TextView mPrevDateTextView;
    private TextView mPrevPlayerNameTextView;

    public interface Callback {
        void onNewBestTimeDonePressed(String playerName, int numLayers);
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.title_new_best_time);

        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_new_best_time, null);

        mCongratsTextView = (TextView) contentView.findViewById(R.id.textView_congrats_record);
        mNewTimeTextView = (TextView) contentView.findViewById(R.id.textView_newTime);
        mNewDateTextView = (TextView) contentView.findViewById(R.id.textView_newDate);
        mNewPlayerNameEditText = (AppCompatEditText) contentView.findViewById(R.id.editText_newPlayerName);
        mPrevTimeTextView = (TextView) contentView.findViewById(R.id.textView_previousTime);
        mPrevDateTextView = (TextView) contentView.findViewById(R.id.textView_previousDate);
        mPrevPlayerNameTextView = (TextView) contentView.findViewById(R.id.textView_previousPlayerName);

        Resources resources = this.getResources();

        Bundle args = this.getArguments();
        final int numLayers = args.getInt("NUM_LAYERS");

        mCongratsTextView.setText(
                String.format(
                        resources.getString(R.string.textview_new_best_time_1),
                        numLayers
                )
        );

        mNewTimeTextView.setText(
                Utils.buildTimeString(
                        this.getContext(),
                        args.getLong("NEW_COMPLETION_TIME"),
                        true
                )
        );

        Date date = new Date(args.getLong("NEW_DATE"));
        String[] timeUnits = date.toString().split(" ");
        mNewDateTextView.setText(
                String.format(
                        resources.getString(R.string.textview_new_best_time_4),
                        timeUnits[1],
                        timeUnits[2],
                        timeUnits[5]
                )
        );

        String strPrevTime = args.getString("PREV_COMPLETION_TIME");

        if (!strPrevTime.equals("")) {
            mPrevTimeTextView.setText(
                    Utils.buildTimeString(
                            this.getContext(),
                            Long.parseLong(strPrevTime),
                            true
                    )
            );
        } else {
            mPrevTimeTextView.setText(this.getString(R.string.not_applicable));
        }

        String strPrevDate = args.getString("PREV_DATE");

        if (!strPrevDate.equals("")) {
            date = new Date(Long.parseLong(strPrevDate));
            timeUnits = date.toString().split(" ");
            mPrevDateTextView.setText(
                    String.format(
                            resources.getString(R.string.textview_new_best_time_4),
                            timeUnits[1],
                            timeUnits[2],
                            timeUnits[5]
                    )
            );
        } else {
            mPrevDateTextView.setText(strPrevDate);
        }

        mPrevPlayerNameTextView.setText(args.getString("PREV_PLAYER_NAME"));

        mNewPlayerNameEditText.setText(resources.getString(R.string.textview_new_best_time_5));
        mNewPlayerNameEditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Editable text = mNewPlayerNameEditText.getText();

                if (text.length() > 0) {
                    text.replace(0, 1, text.subSequence(0, 1), 0, 1);
                    mNewPlayerNameEditText.selectAll();
                }
            }
        });
        mNewPlayerNameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    dismiss();
                    ((NewBestTimeDialogFragment.Callback) getActivity()).onNewBestTimeDonePressed(
                            String.valueOf(mNewPlayerNameEditText.getText()),
                            numLayers
                    );
                    return true;
                }

                return false;
            }
        });

        builder.setView(contentView);

        mAlertDialog = builder.create();

        return mAlertDialog;
    }
}
