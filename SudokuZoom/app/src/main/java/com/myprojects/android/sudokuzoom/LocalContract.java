/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myprojects.android.sudokuzoom;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Defines table and column names for the weather database.
 */
public class LocalContract {

    // The "Content authority" is a name for the entire content provider, similar to the
    // relationship between a domain name and its website.  A convenient string to use for the
    // content authority is the package name for the app, which is guaranteed to be unique on the
    // device.
    public static final String CONTENT_AUTHORITY = "com.myprojects.android.sudokuzoom";

    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // Possible paths (appended to base content URI for possible URI's)
    public static final String PATH_GAME_INFO = "game_info";
    public static final String PATH_TIMED_GAME_PUZZLE_INFO = "timed_game_puzzle_info";
    public static final String PATH_UNTIMED_GAME_PUZZLE_INFO = "untimed_game_puzzle_info";
    public static final String PATH_BEST_TIMES = "best_times";

    public static final class GameInfoEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_GAME_INFO).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_GAME_INFO;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_GAME_INFO;

        // Table name
        public static final String TABLE_NAME = "solo_game_info";

        // Columns
        public static final String COLUMN_TOP_PUZZLE_ID = "top_puzzle_id";
        public static final String COLUMN_START_TIME = "start_time";
        public static final String COLUMN_CURRENT_PUZZLE_ID = "current_puzzle_id";

        public static Uri buildSoloGameInfoUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getStartTimeFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    public static final class TimedGamePuzzleInfoEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_TIMED_GAME_PUZZLE_INFO).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_TIMED_GAME_PUZZLE_INFO;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_TIMED_GAME_PUZZLE_INFO;

        // Table name
        public static final String TABLE_NAME = "timed_game_puzzle_info";

        // Columns
        public static final String COLUMN_PUZZLE_ID = "puzzle_id";
        public static final String COLUMN_CHILD_PUZZLE_IDS = "child_puzzle_ids";
        public static final String COLUMN_SOLUTION = "solution";
        public static final String COLUMN_GIVENS = "givens";
        public static final String COLUMN_ENTERED = "entered";
        public static final String COLUMN_LAYER = "layer";
        public static final String COLUMN_SOLVED = "solved";

        public static Uri buildTimedGamePuzzleInfoUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getPuzzleIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    public static final class UntimedGamePuzzleInfoEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_UNTIMED_GAME_PUZZLE_INFO).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_UNTIMED_GAME_PUZZLE_INFO;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_UNTIMED_GAME_PUZZLE_INFO;

        // Table name
        public static final String TABLE_NAME = "untimed_game_puzzle_info";

        // Columns
        public static final String COLUMN_PUZZLE_ID = "puzzle_id";
        public static final String COLUMN_CHILD_PUZZLE_IDS = "child_puzzle_ids";
        public static final String COLUMN_SOLUTION = "solution";
        public static final String COLUMN_GIVENS = "givens";
        public static final String COLUMN_ENTERED = "entered";
        public static final String COLUMN_LAYER = "layer";
        public static final String COLUMN_SOLVED = "solved";

        public static Uri buildUntimedGamePuzzleInfoUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getPuzzleIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    public static final class BestTimesEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_BEST_TIMES).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_BEST_TIMES;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_BEST_TIMES;

        // Table name
        public static final String TABLE_NAME = "best_times";

        // Columns
        public static final String COLUMN_LAYER_COUNT = "layer_count";
        public static final String COLUMN_COMPLETION_TIME = "completion_time";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_PLAYER_NAME = "player_name";

        public static Uri buildBestTimesUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getLayerCountFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }
}
