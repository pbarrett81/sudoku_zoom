package com.myprojects.android.sudokuzoom;

import android.os.AsyncTask;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Owner on 1/4/2018.
 */

public class HttpDateTask extends AsyncTask<Void, Integer, String> {

    public static final String LOG_TAG = HttpDateTask.class.getSimpleName();
    public static final String TIME_URL = "http://www.google.com";

    protected String doInBackground(Void... params) {
        String unixTime;

        try {
            URL url = new URL(TIME_URL);
            URLConnection httpCon = url.openConnection();
            unixTime = String.valueOf(httpCon.getHeaderFieldDate("Date", -1));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            unixTime = "-1";
            //unixTime = null;
        } catch (IOException e) {
            e.printStackTrace();
            unixTime = "-1";
            //unixTime = null;
        }

        return unixTime;
    }
}
