package com.myprojects.android.sudokuzoom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

/**
 * Created by Owner on 11/19/2017.
 */

public class SubmitAnswerDialogFragment extends DialogFragment {

    private Context mContext;

    private AlertDialog mAlertDialog;

    public interface Callback {
        void onCorrectOkPressed();
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getContext();
        Bundle args = getArguments();

        if (args != null) {
            boolean correct = args.getBoolean("CORRECT");
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

            if (correct) {
                builder.setTitle(R.string.checkmark);

                long elapsedTime = args.getLong("ELAPSED_TIME");

                if (elapsedTime > 0) {
                    builder.setMessage(
                            String.format(
                                    mContext.getString(R.string.correct_message_2),
                                    GameTimer.buildTimeString(
                                            mContext,
                                            elapsedTime,
                                            true
                                    )
                            )
                    );
                } else {
                    builder.setMessage(R.string.correct_message);
                }

                builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dismiss();
                        ((Callback) mContext).onCorrectOkPressed();
                    }
                });
            } else {
                builder.setTitle(R.string.heavy_ballot_x);
                builder.setMessage(R.string.incorrect_message);
                builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dismiss();
                    }
                });
            }

            mAlertDialog = builder.show();
        }

        return mAlertDialog;
    }
}
