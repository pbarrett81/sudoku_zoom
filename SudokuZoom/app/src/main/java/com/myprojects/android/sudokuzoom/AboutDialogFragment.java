package com.myprojects.android.sudokuzoom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Html;

/**
 * Created by Owner on 11/19/2017.
 */

public class AboutDialogFragment extends DialogFragment {

    private Context mContext;

    private AlertDialog mAlertDialog;

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.title_about);

        try {
            builder.setMessage(
                    Html.fromHtml(
                            String.format(
                                    mContext.getString(R.string.message_about),
                                    mContext.getPackageManager().getPackageInfo(
                                            mContext.getPackageName(),
                                            0
                                    ).versionName
                            )
                    )
            );
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dismiss();
            }
        });

        mAlertDialog = builder.create();

        return mAlertDialog;
    }
}
