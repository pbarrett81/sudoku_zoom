package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

import java.util.ArrayList;

/**
 * Created by Owner on 1/19/2018.
 */

public class GameMapLayout extends FrameLayout {

    private Context mContext;

    private boolean mDisplayed = false;
    private boolean mDimensSet = false;

    private int mNumSections = 0;

    private ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener;

    public GameMapLayout(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public GameMapLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public GameMapLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    public void init() {
        mGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {

                if (!mDisplayed) {
                    //Utils.log("onGlobalLayout");
                    removeLayoutListener();
                    mDisplayed = true;
                    setImageDimens();
                }
            }
        };
        addLayoutListener();
    }

    public void addLayoutListener() {
        this.getViewTreeObserver().addOnGlobalLayoutListener(mGlobalLayoutListener);
    }

    public void removeLayoutListener() {

        try {
            this.getViewTreeObserver().removeGlobalOnLayoutListener(mGlobalLayoutListener);
        }  catch (NoSuchMethodError e) {
            e.printStackTrace();
        }
    }

    public void setImageValues(Integer[] sections) {
        ArrayList<AppCompatImageView> imageViews = new ArrayList<>();

        addLayoutListener();
        this.removeAllViews();
        imageViews.add(createImageView(R.drawable.game_map_9));
        mNumSections = 0;

        if (sections != null) {

            for (int i = 0; i < sections.length; ++i) {

                if (sections[i] > -1) {
                    int resId = 0;

                    switch (sections[i]) {

                        case 0:
                            resId = R.drawable.game_map_0;
                            break;

                        case 1:
                            resId = R.drawable.game_map_1;
                            break;

                        case 2:
                            resId = R.drawable.game_map_2;
                            break;

                        case 3:
                            resId = R.drawable.game_map_3;
                            break;

                        case 4:
                            resId = R.drawable.game_map_4;
                            break;

                        case 5:
                            resId = R.drawable.game_map_5;
                            break;

                        case 6:
                            resId = R.drawable.game_map_6;
                            break;

                        case 7:
                            resId = R.drawable.game_map_7;
                            break;

                        case 8:
                            resId = R.drawable.game_map_8;
                            break;
                    }

                    imageViews.add(1, createImageView(resId));
                    mNumSections++;
                }
            }
        }

        for (AppCompatImageView imageView : imageViews) {
            addImageView(imageView);
        }

        mDisplayed = false;
        mDimensSet = false;
        setImageDimens();
    }

    public AppCompatImageView createImageView(int resId) {
        AppCompatImageView newImageView = new AppCompatImageView(mContext);
        newImageView.setImageResource(resId);

        return newImageView;
    }

    public void addImageView(AppCompatImageView imageView) {
        this.addView(imageView, 0);

        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) imageView.getLayoutParams();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        imageView.setLayoutParams(layoutParams);
    }

    public void setImageDimens() {

        if (mDisplayed && !mDimensSet) {
            int childCount = this.getChildCount();

            for (int i = 0; i < childCount; ++i) {
                AppCompatImageView imageView = (AppCompatImageView) this.getChildAt(i);
                float scaleFactor = (float) (this.getHeight() / ((mNumSections + 1) * 0.6 * imageView.getHeight()));
                int newHeight = (int) (imageView.getHeight() * scaleFactor);

                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) imageView.getLayoutParams();
                layoutParams.width = (int) (imageView.getWidth() * scaleFactor);
                layoutParams.height = newHeight;

                if (i < (childCount - 2)) {
                    layoutParams.bottomMargin = (int) ((childCount - 2 - i) * newHeight * 0.573);
                }

                imageView.setLayoutParams(layoutParams);
            }

            mDimensSet = true;
        }
    }
}
