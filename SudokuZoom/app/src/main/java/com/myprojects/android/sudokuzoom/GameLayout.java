package com.myprojects.android.sudokuzoom;

import android.arch.lifecycle.ViewModelProviders;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.text.Html;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

/**
 * Created by Owner on 6/2/2017.
 */

public class GameLayout extends RelativeLayout {

    public static final String LOG_TAG = GameLayout.class.getSimpleName();
    public static final float SCALE_UP = 3f;
    public static final int DEFAULT_DURATION = 1000;

    private Context mContext;
    private Resources mResources;

    private FrameLayout mPuzzleContainer;

    private PuzzleLayout mBaseLayout;
    private PuzzleView mBasePuzzle;
    private PuzzleViewAdapter mBasePuzzleViewAdapter;

    private PuzzleLayout mZoomInLayout;

    private CellPopupWindow mCellPopupWindow;
    private int mPopupWidth;
    private int mPopupHeight;

    private TextView mLayersTextView;

    private TextView mBestTimeTextView;
    private TextView mTimerTextView;
    private TextView mBestTimeValue;
    private TextView mTimerValue;

    private float mPivotX;
    private float mPivotY;

    private int mNumLayers;

    private ArrayList<PuzzleLayout> mPuzzleLayoutList = new ArrayList<>();

    private GameTimer mGameTimer;

    private GameMapLayout mGameMapLayout;
    private Integer[] mSectionsDisplayed = {-1, -1};

    private MainActivityModel mMainActivityModel;

    private LinearLayout mZoomLayout;
    private TextView mZoomMessageTextView;
    private TextView mZoomSymbolTextView;

    private Button mClearAllButton;
    private Button mSubmitAnswerButton;

    private TextView mInstructionsTextView;

    public interface Callback {
        void onAnswerChecked(boolean correct, boolean topPuzzle);
        void onComplete(int numLayers);
        void onClearAllKeyPressed();
        void onSubmitAnswerKeyPressed();
    }

    public GameLayout(Context context) {
        super(context);
        mContext = context;
    }

    public GameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public void setGameTimer(GameTimer gameTimer) {
        mGameTimer = gameTimer;
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();

        mResources = mContext.getResources();
        mPuzzleContainer = (FrameLayout) this.findViewById(R.id.puzzle_container);

        mClearAllButton = (Button) this.findViewById(R.id.clear_keyboard);
        mClearAllButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dismissCellPopup();
                ((Callback) mContext).onClearAllKeyPressed();
            }
        });

        mSubmitAnswerButton = (Button) this.findViewById(R.id.answer_keyboard);
        mSubmitAnswerButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dismissCellPopup();
                ((Callback) mContext).onSubmitAnswerKeyPressed();
            }
        });

        mLayersTextView = (TextView) this.findViewById(R.id.layers_textview);

        mTimerTextView = (TextView) this.findViewById(R.id.timer_textview);
        mBestTimeTextView = (TextView) this.findViewById(R.id.best_time_textview);
        mTimerValue = (TextView) this.findViewById(R.id.timer_value);
        mBestTimeValue = (TextView) this.findViewById(R.id.best_time_value);

        mGameMapLayout = (GameMapLayout) this.findViewById(R.id.game_map_layout);

        mZoomLayout = (LinearLayout) this.findViewById(R.id.layout_zoom);
        mZoomSymbolTextView = (TextView) this.findViewById(R.id.textview_zoom_symbol);
        mZoomMessageTextView = (TextView) this.findViewById(R.id.textview_zoom_message);

        mInstructionsTextView = (TextView) this.findViewById(R.id.textview_instructions);

        mMainActivityModel = ViewModelProviders.of((AppCompatActivity) mContext).get(MainActivityModel.class);

        mCellPopupWindow = new CellPopupWindow(mContext);
        mPopupWidth = mCellPopupWindow.getPopupWidth();
        mPopupHeight = mCellPopupWindow.getPopupHeight();
    }

    public void buildDisplay(int bottomDimen) {
        mPuzzleContainer.removeAllViews();

        PuzzleData[] puzzleDataArray = mMainActivityModel.getPuzzleDataArray();
        mNumLayers = puzzleDataArray[puzzleDataArray.length - 1].getLayerNum();
        mPuzzleLayoutList = new ArrayList<>();

        for (PuzzleData puzzleData : puzzleDataArray) {
            PuzzleLayout puzzleLayout = new PuzzleLayout(mContext);
            PuzzleView puzzleView = puzzleLayout.getPuzzleView();
            PuzzleViewAdapter puzzleViewAdapter = new PuzzleViewAdapter(mContext, puzzleData);

            puzzleView.setLayoutManager(new GridLayoutManager(mContext, 9));
            puzzleView.setAdapter(puzzleViewAdapter);
            puzzleView.init();

            mPuzzleLayoutList.add(puzzleLayout);
        }

        int padding = mPuzzleContainer.getPaddingLeft();

        bottomDimen -= (2 * padding);

        int bottomBorderWidth = mResources.getDimensionPixelSize(R.dimen.puzzle_stroke_wide);

        for (int i = 0; i < mPuzzleLayoutList.size(); ++i) {
            PuzzleLayout puzzleLayout = mPuzzleLayoutList.get(i);

            if (i == 0) {
                puzzleLayout.initDisplay(bottomDimen,0,0);
                puzzleLayout.setScaledBorderWidth(SCALE_UP * (float) bottomBorderWidth);

                ViewGroup.LayoutParams layoutParams = mPuzzleContainer.getLayoutParams();
                int puzzleWidth = puzzleLayout.getLayoutParams().width;

                layoutParams.width = puzzleWidth + (2 * padding);
                layoutParams.height = puzzleWidth + (2 * padding);
                mPuzzleContainer.setLayoutParams(layoutParams);

                mPuzzleContainer.addView(puzzleLayout, 0);
                setBaseState(puzzleLayout);
            } else {
                PuzzleViewAdapter puzzleViewAdapter =
                        (PuzzleViewAdapter) puzzleLayout.getPuzzleView().getAdapter();
                int layer = puzzleViewAdapter.getPuzzleData().getLayerNum();
                float totalBorderWidth = 0;

                for (int j = 1; j < layer; ++j) {
                    totalBorderWidth += findPuzzleByLayer(j).getScaledBorderWidth();
                }

                PuzzleLayout parentLayout = findPuzzleByLayer(layer - 1);
                puzzleLayout.initDisplay(
                        parentLayout.getLayoutParams().width,
                        parentLayout.getScaledBorderWidth(),
                        totalBorderWidth
                );

                puzzleLayout.setScaledBorderWidth(
                        SCALE_UP *
                                (float) bottomBorderWidth *
                                ((float) puzzleLayout.getLayoutParams().width / (float) bottomDimen)
                );
            }

            puzzleLayout.setSectionColors();
        }

        if (mGameTimer.getStartTime() > 0) {
            mTimerTextView.setText(mContext.getString(R.string.textview_timer));
            mGameTimer.setTimerValue(mTimerValue);
            mTimerTextView.setVisibility(VISIBLE);
            mTimerValue.setVisibility(VISIBLE);

            DBQueryTask dbQueryTask = new DBQueryTask();
            dbQueryTask.execute(
                    new DBQueryTask.Data(
                            mContext,
                            LocalContract.BestTimesEntry.CONTENT_URI,
                            null,
                            LocalContract.BestTimesEntry.COLUMN_LAYER_COUNT + " = ?",
                            new String[]{String.valueOf(mNumLayers)},
                            null
                    )
            );

            Cursor cursor = null;

            try {
                cursor = dbQueryTask.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            if (cursor != null) {
                cursor.moveToFirst();
                mBestTimeTextView.setText(mResources.getString(R.string.textview_best_time));

                if (cursor.getCount() > 0) {
                    long bestTime = Long.parseLong(
                            cursor.getString(
                                    cursor.getColumnIndex(
                                            LocalContract.BestTimesEntry.COLUMN_COMPLETION_TIME
                                    )
                            )
                    );
                    mBestTimeValue.setText(GameTimer.buildTimeString(mContext, bestTime, true));
                } else {
                    mBestTimeValue.setText(mResources.getString(R.string.not_applicable));
                }

                cursor.close();
                mBestTimeTextView.setVisibility(VISIBLE);
                mBestTimeValue.setVisibility(VISIBLE);
            }
        }

        mLayersTextView.setText(
                Html.fromHtml(
                        String.format(
                                mContext.getString(R.string.layers_format),
                                String.valueOf(mNumLayers)
                        )
                )
        );

        if (mNumLayers > 1) {
            mGameMapLayout.setImageValues(null);
            mInstructionsTextView.setVisibility(VISIBLE);
        } else {
            mInstructionsTextView.setVisibility(INVISIBLE);
        }

        int currentPuzzleId = mMainActivityModel.getCurrentPuzzleId();
        ArrayList<Integer> sections = new ArrayList<>();
        int topPuzzleId = puzzleDataArray[0].getPuzzleId();
        int puzzleCount = 0;

        while (currentPuzzleId != topPuzzleId) {

            if (puzzleCount < puzzleDataArray.length) {
                PuzzleData currPuzzleData = puzzleDataArray[puzzleCount];
                int currSection = currPuzzleData.findChildSection(currentPuzzleId);

                if (currSection != -1) {
                    currentPuzzleId = currPuzzleData.getPuzzleId();
                    sections.add(0, currSection);
                    puzzleCount = 0;
                } else {
                    puzzleCount++;
                }
            }
        }

        for (int i = 0; i < sections.size(); ++i) {
            zoomInPuzzle(sections.get(i), 0);
        }
    }

    public void setBaseState(PuzzleLayout puzzleLayout) {

        if (puzzleLayout != null) {
            mBaseLayout = puzzleLayout;
            mBasePuzzle = mBaseLayout.getPuzzleView();
            mBasePuzzleViewAdapter = (PuzzleViewAdapter) mBasePuzzle.getAdapter();
            mBasePuzzle.setGestureListening(true);
        }

        if (mBasePuzzleViewAdapter.getPuzzleData().checkSolved()) {
            mBaseLayout.showComplete();
            mClearAllButton.setVisibility(INVISIBLE);
            mSubmitAnswerButton.setVisibility(INVISIBLE);
        } else {
            mClearAllButton.setVisibility(VISIBLE);
            mSubmitAnswerButton.setVisibility(VISIBLE);}
    }

    public void showCellPopupWindow(int animStyle, int x_coord, int y_coord) {
        dismissCellPopup();
        mCellPopupWindow.setAnimationStyle(animStyle);
        mCellPopupWindow.showAtLocation(this, Gravity.NO_GRAVITY, x_coord, y_coord);
    }

    public void updateCurrentPuzzleId(int puzzleId) {
        mMainActivityModel.setCurrentPuzzleId(puzzleId);

        ContentValues contentValues = new ContentValues();
        contentValues.put(
                LocalContract.GameInfoEntry.COLUMN_CURRENT_PUZZLE_ID,
                String.valueOf(puzzleId)
        );

        Intent intent = new Intent(MainActivity.ACTION_DB_UPDATE);
        intent.putExtra(
                "com.myprojects.android.sudokuzoom.Uri",
                LocalContract.GameInfoEntry.buildSoloGameInfoUri(mGameTimer.getStartTime())
        );
        intent.putExtra("com.myprojects.android.sudokuzoom.ContentValues", contentValues);
        intent.putExtra("com.myprojects.android.sudokuzoom.Selection","");
        intent.putExtra(
                "com.myprojects.android.sudokuzoom.SelectionArgs",
                new String[]{}
        );

        DatabaseService.enqueueWork(mContext, intent);
    }

    public void zoomInPuzzle(final int section, final int duration) {

        if (!mBaseLayout.getComplete() && section != -1) {
            dismissCellPopup();
            final PuzzleViewAdapter basePuzzleViewAdapter = (PuzzleViewAdapter) mBasePuzzle.getAdapter();
            mZoomInLayout = findPuzzleById(basePuzzleViewAdapter.getPuzzleData().getChildPuzzleId(section));

            if (mZoomInLayout != null) {

                if (!basePuzzleViewAdapter.getPuzzleData().checkSolved()) {

                    if (duration > 0) {
                        mZoomLayout.getViewTreeObserver().addOnGlobalLayoutListener(
                                new ViewTreeObserver.OnGlobalLayoutListener() {

                                    @Override
                                    public void onGlobalLayout() {
                                        mZoomLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                        mBaseLayout.startAnimation(initZoomIn(section, duration));
                                        setBaseState(mZoomInLayout);
                                        updateCurrentPuzzleId(mBasePuzzleViewAdapter.getPuzzleData().getPuzzleId());
                                    }
                                });
                        mZoomSymbolTextView.setText(mContext.getString(R.string.zoom_in_symbol));
                        mZoomMessageTextView.setText(
                                String.format(
                                        mContext.getString(R.string.zoom_in_message),
                                        ((PuzzleViewAdapter) mZoomInLayout.getPuzzleView().getAdapter())
                                                .getPuzzleData().getLayerNum()
                                )
                        );
                        mZoomLayout.setVisibility(VISIBLE);
                    } else {
                        mBaseLayout.startAnimation(initZoomIn(section, duration));
                        setBaseState(mZoomInLayout);
                        addDisplayedSection(section);
                    }
                }
            }
        }
    }

    public AnimationSet initZoomIn(final int section, final int duration) {
        mPuzzleContainer.addView(mZoomInLayout, 0);
        ((PuzzleViewAdapter) mBasePuzzle.getAdapter()).setCurrentSection(section);

        findPivots(section);

        ScaleAnimation scaleAnim = new ScaleAnimation(
                1f,
                3f,
                1f,
                3f,
                Animation.RELATIVE_TO_SELF,
                mPivotX,
                Animation.RELATIVE_TO_SELF,
                mPivotY
        );
        scaleAnim.setInterpolator(new DecelerateInterpolator());

        CellTranslateAnimation cellTranslateAnim =
                new CellTranslateAnimation(1, mBasePuzzle);
        cellTranslateAnim.setStartOffset(DEFAULT_DURATION);

        AnimationSet animSet = new AnimationSet(false);
        animSet.addAnimation(scaleAnim);
        animSet.addAnimation(cellTranslateAnim);
        animSet.setDuration(duration);
        animSet.setFillAfter(true);

        animSet.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mZoomLayout.setVisibility(INVISIBLE);

                if (duration > 0) {
                    addDisplayedSection(section);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mBasePuzzle.setGestureListening(false);

        return animSet;
    }

    public void zoomOutPuzzle() {

        if (mPuzzleContainer.getChildCount() > 1) {
            setBaseState((PuzzleLayout) mPuzzleContainer.getChildAt(1));

            mZoomLayout.getViewTreeObserver().addOnGlobalLayoutListener(
                    new ViewTreeObserver.OnGlobalLayoutListener() {

                        @Override
                        public void onGlobalLayout() {
                            mZoomLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            dismissCellPopup();

                            final PuzzleViewAdapter basePuzzleViewAdapter = (PuzzleViewAdapter) mBasePuzzle.getAdapter();

                            CellTranslateAnimation cellShiftAnim =
                                    new CellTranslateAnimation(-1, mBasePuzzle);

                            findPivots(basePuzzleViewAdapter.getCurrentSection());

                            ScaleAnimation scaleAnim = new ScaleAnimation(
                                    3f,
                                    1f,
                                    3f,
                                    1f,
                                    Animation.RELATIVE_TO_SELF,
                                    mPivotX,
                                    Animation.RELATIVE_TO_SELF,
                                    mPivotY
                            );
                            scaleAnim.setStartOffset(DEFAULT_DURATION);
                            scaleAnim.setInterpolator(new DecelerateInterpolator());

                            AnimationSet animSet = new AnimationSet(false);
                            animSet.addAnimation(cellShiftAnim);
                            animSet.addAnimation(scaleAnim);
                            animSet.setDuration(DEFAULT_DURATION);

                            scaleAnim.setAnimationListener(new Animation.AnimationListener() {

                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    new Handler().post(new Runnable() {

                                        public void run() {
                                            mPuzzleContainer.removeViewAt(0);
                                        }
                                    });
                                    removeDisplayedSection();
                                    mZoomLayout.setVisibility(INVISIBLE);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            mBaseLayout.startAnimation(animSet);
                            updateCurrentPuzzleId(mBasePuzzleViewAdapter.getPuzzleData().getPuzzleId());
                        }
                    });
            mZoomSymbolTextView.setText(mContext.getString(R.string.zoom_out_symbol));
            mZoomMessageTextView.setText(
                    String.format(
                            mContext.getString(R.string.zoom_out_message),
                            mBasePuzzleViewAdapter.getPuzzleData().getLayerNum()
                    )
            );
            mZoomLayout.setVisibility(VISIBLE);
        }
    }

    public void addDisplayedSection(int section) {

        if (mNumLayers > 1) {
            int index = Arrays.asList(mSectionsDisplayed).indexOf(-1);

            if (index != -1) {
                mSectionsDisplayed[index] = section;
            }

            mGameMapLayout.setImageValues(mSectionsDisplayed);
        }
    }

    public void removeDisplayedSection() {

        if (mNumLayers > 1) {
            int index = Arrays.asList(mSectionsDisplayed).indexOf(-1);

            if (index == -1 || index > 0) {

                if (index == -1) {
                    index = mSectionsDisplayed.length;
                }

                mSectionsDisplayed[index - 1] = -1;
            }

            mGameMapLayout.setImageValues(mSectionsDisplayed);
        }
    }

    public PuzzleLayout findPuzzleById(int id) {

        for (PuzzleLayout puzzleLayout : mPuzzleLayoutList) {
            PuzzleViewAdapter puzzleViewAdapter =
                    (PuzzleViewAdapter) puzzleLayout.getPuzzleView().getAdapter();

            if (puzzleViewAdapter.getPuzzleData().getPuzzleId() == id) {
                return puzzleLayout;
            }
        }

        return null;
    }

    public PuzzleLayout findPuzzleByLayer(int layer) {

        for (PuzzleLayout puzzleLayout : mPuzzleLayoutList) {
            PuzzleViewAdapter puzzleViewAdapter =
                    (PuzzleViewAdapter) puzzleLayout.getPuzzleView().getAdapter();

            if (puzzleViewAdapter.getPuzzleData().getLayerNum() == layer) {
                return puzzleLayout;
            }
        }

        return null;
    }

    public void findPivots(int section) {

        switch (section) {

            case 0:
                mPivotX = 0f;
                mPivotY = 0f;
                break;

            case 1:
                mPivotX = 0.5f;
                mPivotY = 0f;
                break;

            case 2:
                mPivotX = 1f;
                mPivotY = 0f;
                break;

            case 3:
                mPivotX = 0f;
                mPivotY = 0.5f;
                break;

            case 4:
                mPivotX = 0.5f;
                mPivotY = 0.5f;
                break;

            case 5:
                mPivotX = 1f;
                mPivotY = 0.5f;
                break;

            case 6:
                mPivotX = 0f;
                mPivotY = 1f;
                break;

            case 7:
                mPivotX = 0.5f;
                mPivotY = 1f;
                break;

            case 8:
                mPivotX = 1f;
                mPivotY = 1f;
                break;

            default:
                break;
        }
    }

    public void onCellClicked(CellLayout cellLayout, int section) {

        if (!mBaseLayout.getComplete()) {
            int animStyle = -1;
            int[] location = new int[2];

            cellLayout.getLocationInWindow(location);

            switch (section) {

                case 0:
                    animStyle = R.style.animation_popup_top_left;
                    location[0] += cellLayout.getWidth();
                    location[1] += cellLayout.getHeight();
                    break;

                case 1:
                    animStyle = R.style.animation_popup_top_left;
                    location[0] += cellLayout.getWidth();
                    location[1] += cellLayout.getHeight();
                    break;

                case 2:
                    animStyle = R.style.animation_popup_top_right;
                    location[0] -= mPopupWidth;
                    location[1] += cellLayout.getHeight();
                    break;

                case 3:
                    animStyle = R.style.animation_popup_bottom_left;
                    location[0] += cellLayout.getWidth();
                    location[1] -= mPopupHeight;
                    break;

                case 4:
                    animStyle = R.style.animation_popup_bottom_left;
                    location[0] += cellLayout.getWidth();
                    location[1] -= mPopupHeight;
                    break;

                case 5:
                    animStyle = R.style.animation_popup_bottom_right;
                    location[0] -= mPopupWidth;
                    location[1] -= mPopupHeight;
                    break;

                case 6:
                    animStyle = R.style.animation_popup_bottom_left;
                    location[0] += cellLayout.getWidth();
                    location[1] -= mPopupHeight;
                    break;

                case 7:
                    animStyle = R.style.animation_popup_bottom_left;
                    location[0] += cellLayout.getWidth();
                    location[1] -= mPopupHeight;
                    break;

                case 8:
                    animStyle = R.style.animation_popup_bottom_right;
                    location[0] -= mPopupWidth;
                    location[1] -= mPopupHeight;
                    break;
            }

            mCellPopupWindow.setNumberPickerValue(
                    Integer.parseInt((String) cellLayout.getTextView().getText())
            );
            showCellPopupWindow(animStyle, location[0], location[1]);
        }
    }

    public void onNumKeyPressed(int num) {
        mBasePuzzle.onNumKeyPressed(num, mGameTimer.getStartTime() > 0);
    }

    public void onClearKeyPressed() {
        mBasePuzzle.onNumKeyPressed(0, mGameTimer.getStartTime() > 0);
    }

    public void onSubmitAnswerKeyPressed() {
        boolean correct = mBasePuzzle.checkAnswer();
        boolean topPuzzle = false;

        if (correct) {
            setBaseState(null);

            ContentValues contentValues = new ContentValues();
            boolean gameComplete = false;

            if (mPuzzleContainer.getChildCount() == 1) {
                mGameTimer.stop();
                topPuzzle = true;

                Intent intent = new Intent(MainActivity.ACTION_DB_DELETE);
                intent.putExtra(
                        "com.myprojects.android.sudokuzoom.Uri",
                        LocalContract.GameInfoEntry.CONTENT_URI
                );
                intent.putExtra(
                        "com.myprojects.android.sudokuzoom.Selection",
                        LocalContract.GameInfoEntry.COLUMN_TOP_PUZZLE_ID + " = ?"
                );
                intent.putExtra(
                        "com.myprojects.android.sudokuzoom.SelectionArgs",
                        new String[] {
                                String.valueOf(mBasePuzzleViewAdapter.getPuzzleData().getPuzzleId()
                                )
                        }
                        );

                DatabaseService.enqueueWork(mContext.getApplicationContext(), intent);

                gameComplete = true;
            }

            if (mGameTimer.getStartTime() > 0) {

                if (gameComplete) {
                    Intent intent = new Intent(MainActivity.ACTION_DB_DELETE);
                    intent.putExtra(
                            "com.myprojects.android.sudokuzoom.Uri",
                            LocalContract.TimedGamePuzzleInfoEntry.CONTENT_URI
                    );
                    intent.putExtra("com.myprojects.android.sudokuzoom.Selection","");
                    intent.putExtra(
                            "com.myprojects.android.sudokuzoom.SelectionArgs",
                            new String[]{}
                    );

                    DatabaseService.enqueueWork(mContext.getApplicationContext(), intent);
                } else {
                    contentValues.put(LocalContract.TimedGamePuzzleInfoEntry.COLUMN_SOLVED, 1);

                    Intent intent = new Intent(MainActivity.ACTION_DB_UPDATE);
                    intent.putExtra(
                            "com.myprojects.android.sudokuzoom.Uri",
                            LocalContract.TimedGamePuzzleInfoEntry.buildTimedGamePuzzleInfoUri(
                                    mBasePuzzleViewAdapter.getPuzzleData().getPuzzleId()
                            )
                    );
                    intent.putExtra("com.myprojects.android.sudokuzoom.ContentValues",contentValues);
                    intent.putExtra("com.myprojects.android.sudokuzoom.Selection","");
                    intent.putExtra(
                            "com.myprojects.android.sudokuzoom.SelectionArgs",
                            new String[]{}
                    );

                    DatabaseService.enqueueWork(mContext.getApplicationContext(), intent);
                }
            } else {

                if (gameComplete) {
                    Intent intent = new Intent(MainActivity.ACTION_DB_DELETE);
                    intent.putExtra(
                            "com.myprojects.android.sudokuzoom.Uri",
                            LocalContract.UntimedGamePuzzleInfoEntry.CONTENT_URI
                    );
                    intent.putExtra("com.myprojects.android.sudokuzoom.Selection","");
                    intent.putExtra(
                            "com.myprojects.android.sudokuzoom.SelectionArgs",
                            new String[]{}
                    );

                    DatabaseService.enqueueWork(mContext.getApplicationContext(), intent);
                } else {
                    contentValues.put(LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_SOLVED, 1);

                    Intent intent = new Intent(MainActivity.ACTION_DB_UPDATE);
                    intent.putExtra(
                            "com.myprojects.android.sudokuzoom.Uri",
                            LocalContract.UntimedGamePuzzleInfoEntry.buildUntimedGamePuzzleInfoUri(
                                    mBasePuzzleViewAdapter.getPuzzleData().getPuzzleId()
                            )
                    );
                    intent.putExtra("com.myprojects.android.sudokuzoom.ContentValues",contentValues);
                    intent.putExtra("com.myprojects.android.sudokuzoom.Selection","");
                    intent.putExtra(
                            "com.myprojects.android.sudokuzoom.SelectionArgs",
                            new String[]{}
                    );

                    DatabaseService.enqueueWork(mContext.getApplicationContext(), intent);
                }
            }
        }

        ((Callback) mContext).onAnswerChecked(correct, topPuzzle);
    }

    public void onCorrectOkPressed() {

        if (mPuzzleContainer.getChildCount() > 1) {
            setBaseState((PuzzleLayout) mPuzzleContainer.getChildAt(1));

            final AnimationSet animSet = new AnimationSet(false);

            CellTranslateAnimation cellTranslateAnim =
                    new CellTranslateAnimation(-1, mBasePuzzle);

            findPivots(mBasePuzzleViewAdapter.getCurrentSection());

            ScaleAnimation scaleAnim = new ScaleAnimation(
                    3f,
                    1f,
                    3f,
                    1f,
                    Animation.RELATIVE_TO_SELF,
                    mPivotX,
                    Animation.RELATIVE_TO_SELF,
                    mPivotY
            );
            scaleAnim.setStartOffset(DEFAULT_DURATION + 500);
            scaleAnim.setInterpolator(new DecelerateInterpolator());

            animSet.addAnimation(cellTranslateAnim);
            animSet.addAnimation(scaleAnim);
            animSet.setDuration(DEFAULT_DURATION);
            animSet.setFillAfter(true);

            cellTranslateAnim.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mBasePuzzleViewAdapter.setCurrentSectionToSolved();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            animSet.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    new Handler().post(new Runnable() {

                        public void run() {
                            mPuzzleContainer.removeViewAt(0);
                        }
                    });
                    removeDisplayedSection();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            mBaseLayout.startAnimation(animSet);
        } else {
            mGameTimer.stop();
            mGameTimer.setTimerValue(null);
            ((Callback) mContext).onComplete(mNumLayers);
        }

        updateCurrentPuzzleId(mBasePuzzleViewAdapter.getPuzzleData().getPuzzleId());
    }

    public void onConfirmYesPressed() {
        mBasePuzzle.clearAllEntries(mGameTimer.getStartTime() > 0);
    }

    public void dismissCellPopup() {

        if (mCellPopupWindow != null) {
            mCellPopupWindow.dismiss();
        }
    }

    public void destroy() {
        mCellPopupWindow.dismiss();
    }
}