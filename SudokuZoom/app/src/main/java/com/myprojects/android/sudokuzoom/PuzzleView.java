package com.myprojects.android.sudokuzoom;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;

/**
 * Created by Owner on 5/31/2017.
 */

public class PuzzleView extends RecyclerView {

    public static final String LOG_TAG = PuzzleView.class.getSimpleName();

    private Context mContext;

    private PuzzleViewAdapter mPuzzleViewAdapter;
    private CellLayout mCurrentCellLayout;

    private GestureDetector mGestureDetector;
    private GestureListener mGestureListener;
    private boolean mGestureListening = true;

    private boolean mDisplayed = false;

    public interface Callback {

        void onPuzzleZoomIn(int section);

        void onPuzzleZoomOut();

        void onCellClicked(CellLayout cellLayout, int section);
    }

    public PuzzleView(Context context) {
        super(context);
        mContext = context;
    }

    public PuzzleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public PuzzleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
    }

    public void init() {
        mPuzzleViewAdapter = (PuzzleViewAdapter) this.getAdapter();

        final PuzzleView puzzleView = this;

        puzzleView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {

                if (!mDisplayed) {
                    puzzleView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    display();
                    mDisplayed = true;
                }
            }
        });
    }

    public void setGestureListening(boolean b) {
        mGestureListening = b;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void display() {
        mPuzzleViewAdapter.setStrokeWidths();
        mPuzzleViewAdapter.setTextSizes();

        setGestureListeners();
    }

    public void setGestureListeners() {
        mGestureListener = new GestureListener();
        mGestureDetector = new GestureDetector(mContext, mGestureListener);
        mGestureDetector.setOnDoubleTapListener(mGestureListener);
    }

    public void onNumKeyPressed(int num, boolean timed) {

        if (mCurrentCellLayout != null) {
            mCurrentCellLayout.updateEntered(num);
            updateEnteredString(timed);
        }
    }

    public void onClearKeyPressed() {

        if (mCurrentCellLayout != null) {
            mCurrentCellLayout.putText("0");
        }
    }

    public void clearAllEntries(boolean timed) {
        mPuzzleViewAdapter.clearAllEntries();
        updateEnteredString(timed);
    }

    public void updateEnteredString(boolean timed) {
        ContentValues contentValues = new ContentValues();
        Intent intent = new Intent(MainActivity.ACTION_DB_UPDATE);

        if (timed) {
            contentValues.put(
                    LocalContract.TimedGamePuzzleInfoEntry.COLUMN_ENTERED,
                    mPuzzleViewAdapter.getPuzzleData().buildEnteredString()
            );

            intent.putExtra(
                    "com.myprojects.android.sudokuzoom.Uri",
                    LocalContract.TimedGamePuzzleInfoEntry.buildTimedGamePuzzleInfoUri(
                            mPuzzleViewAdapter.getPuzzleData().getPuzzleId()
                    )
            );
        } else {
            contentValues.put(
                    LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_ENTERED,
                    mPuzzleViewAdapter.getPuzzleData().buildEnteredString()
            );

            intent.putExtra(
                    "com.myprojects.android.sudokuzoom.Uri",
                    LocalContract.UntimedGamePuzzleInfoEntry.buildUntimedGamePuzzleInfoUri(
                            mPuzzleViewAdapter.getPuzzleData().getPuzzleId()
                    )
            );
        }

        intent.putExtra("com.myprojects.android.sudokuzoom.ContentValues",contentValues);
        intent.putExtra("com.myprojects.android.sudokuzoom.Selection","");
        intent.putExtra(
                "com.myprojects.android.sudokuzoom.SelectionArgs",
                new String[]{}
        );

        DatabaseService.enqueueWork(mContext.getApplicationContext(), intent);
        //DatabaseService.enqueueWork(mContext, intent);
    }

    public boolean checkAnswer() {
        boolean isCorrect = mPuzzleViewAdapter.getPuzzleData().checkAnswer();
        mGestureListening = !isCorrect;
        return isCorrect;
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        CellLayout cellLayout = (CellLayout) findChildViewUnder(e.getX(), e.getY());

        if (mGestureListening && cellLayout != null) {

            if (!cellLayout.getCellData().getSolved()) {
                cellLayout.setSelected(true);

                if (mCurrentCellLayout != null && mCurrentCellLayout != cellLayout) {
                    mCurrentCellLayout.setSelected(false);
                }

                mCurrentCellLayout = cellLayout;
            }

            mGestureDetector.onTouchEvent(e);
            return true;
        } else {
            return false;
        }
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            CellLayout cellLayout = (CellLayout) findChildViewUnder(e.getX(), e.getY());

            if (cellLayout != null) {

                if (!cellLayout.getCellData().getSolved()) {
                    ((Callback) mContext).onCellClicked(
                            mCurrentCellLayout,
                            mPuzzleViewAdapter.getPuzzleData().findSection(mCurrentCellLayout)
                    );
                }
            }

            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            //Utils.log("onDoubleTap");
            CellLayout cellLayout = (CellLayout) findChildViewUnder(e.getX(), e.getY());

            if (cellLayout != null) {
                ((Callback) mContext).onPuzzleZoomIn(mPuzzleViewAdapter.getPuzzleData().findSection(cellLayout));
            }

            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            //Utils.log("onFling");

            ((Callback) mContext).onPuzzleZoomOut();

            return true;
        }
    }
}
