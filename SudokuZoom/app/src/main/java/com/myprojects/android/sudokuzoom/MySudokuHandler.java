package com.myprojects.android.sudokuzoom;

import java.util.List;

import dlx.DancingLinks.DancingNode;
import dlx.SudokuHandler;

/**
 * Created by Owner on 7/11/2017.
 */

public class MySudokuHandler extends SudokuHandler {

    public static final String LOG_TAG = MySudokuHandler.class.getSimpleName();

    private int[][] mSudoku;

    public MySudokuHandler(int boardSize) {
        super(boardSize);
    }

    public int[][] getSudoku() {
        return mSudoku;
    }

    @Override
    public void handleSolution(List<DancingNode> answer) {

        if (mSudoku == null) {
            mSudoku = parseBoard(answer);
        }

        /*for (int i = 0; i < mSudoku.length; ++i) {
            Log.d(LOG_TAG, "mSudoku[" + String.valueOf(i) + "] = " + Arrays.toString(mSudoku[i]));
        }*/
    }
}
