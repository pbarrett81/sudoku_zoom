package com.myprojects.android.sudokuzoom;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by Owner on 10/26/2017.
 */

public class CellTranslateAnimation extends Animation {

    private final float DISTANCE = 3.5f;

    private int mDirection;
    private PuzzleView mPuzzleView;
    private CellTranslateDecoration mPreviousDecoration;

    public CellTranslateAnimation(int direction, PuzzleView puzzleView) {
        mDirection = direction;
        mPuzzleView = puzzleView;
    }

    @Override
    public void applyTransformation (float interpolatedTime, Transformation t) {
        int shift = (int) (mDirection * interpolatedTime * DISTANCE * (mPuzzleView.getWidth() / 9));
        CellTranslateDecoration itemDecoration = new CellTranslateDecoration(shift);

        if (mPreviousDecoration != null) {
            mPuzzleView.removeItemDecoration(mPreviousDecoration);
        }

        mPuzzleView.addItemDecoration(itemDecoration);
        mPreviousDecoration = itemDecoration;
    }
}
