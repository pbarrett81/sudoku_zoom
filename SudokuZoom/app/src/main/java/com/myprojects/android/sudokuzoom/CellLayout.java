package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by Owner on 5/29/2017.
 */

public class CellLayout extends FrameLayout {

    public static final String LOG_TAG = PuzzleView.class.getSimpleName();
    private final int STANDARD_CELL_WIDTH = 77;

    private Context mContext;
    private Resources mResources;

    private AutoSizingTextView mTextView;
    private ImageView mSelectedView;

    private Drawable mDefaultBackgroundDrawable;

    private float mStrokeWidth = 0;

    private CellData mCellData;

    private int mGivenColor;

    public CellLayout(Context context) {
        super(context);
        mContext = context;
    }

    public CellLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CellLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();

        mResources = mContext.getResources();

        mDefaultBackgroundDrawable = ResourcesCompat.getDrawable(mResources, R.drawable.background_cell, null);
        ViewCompat.setBackground(this, mDefaultBackgroundDrawable);

        mTextView = (AutoSizingTextView) this.findViewById(R.id.textView);

        mSelectedView = new ImageView(mContext);
        mSelectedView.setImageResource(R.drawable.background_cell_selected);
    }

    public void init(int givenColor) {
        mGivenColor = givenColor;

        if (mCellData.getGiven()) {
            this.setGiven();

            if (mCellData.getSolved()) {
                putText(String.valueOf(mCellData.getAnswer()));
            } else {
                putText(String.valueOf(mCellData.getEntered()));
            }
        } else {
            putText(String.valueOf(mCellData.getEntered()));
        }
    }

    public AutoSizingTextView getTextView() {
        return mTextView;
    }

    public CellData getCellData() {
        return mCellData;
    }

    public void setCellData(CellData cellData) {
        mCellData = cellData;
    }

    public void setStrokeWidth(float strokeWidth) {
        mStrokeWidth = strokeWidth;

        int intStrokeWidth = Math.round(strokeWidth);

        if (intStrokeWidth == 0) {
            intStrokeWidth = 1;
        }

        Drawable backgroundDrawable = this.getBackground();
        GradientDrawable baseDrawable;

        if (backgroundDrawable instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) backgroundDrawable;
            baseDrawable = (GradientDrawable) layerDrawable.getDrawable(0);
            layerDrawable.setLayerInset(
                    1,
                    intStrokeWidth,
                    intStrokeWidth,
                    intStrokeWidth,
                    intStrokeWidth
            );
        } else {
            baseDrawable = (GradientDrawable) backgroundDrawable;
        }

        baseDrawable.setStroke(
                intStrokeWidth,
                mResources.getColor(R.color.colorCellStroke2)
        );
    }

    public float determineStrokeWidth() {
        return (float) (mResources.getDimensionPixelSize(R.dimen.puzzle_stroke_thin) * this.getWidth())
                / STANDARD_CELL_WIDTH;
    }

    public void setTextSize(float textSize) {
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
    }

    public float determineTextSize() {
        return this.getHeight() - mTextView.getPaddingTop() - mTextView.getPaddingBottom();
    }

    public void setSelected(boolean b) {
        int index = this.indexOfChild(mSelectedView);

        if (b && index == -1) {
            this.addView(mSelectedView, 0);
        } else if (!b && index != -1) {
            this.removeViewAt(index);
        }
    }

    public void setGiven() {

        if (mCellData.getSolved()) {
            setOverlayColor(mGivenColor);
            mTextView.setTypeface(null, Typeface.BOLD);
        } else {
            setOverlayColor(mContext.getResources().getColor(R.color.colorCellFillGiven));
            mTextView.setTypeface(null, Typeface.NORMAL);
        }
    }

    public void setOverlayColor(int color) {
        GradientDrawable overlayDrawable = new GradientDrawable();
        overlayDrawable.setColor(
                Color.argb(
                        Math.round(Color.alpha(color) * 0.5f),
                        Color.red(color),
                        Color.green(color),
                        Color.blue(color)
                )
        );

        Drawable[] layers = {mDefaultBackgroundDrawable, overlayDrawable};
        LayerDrawable layerDrawable = new LayerDrawable(layers);
        int strokeWidth = (int) mStrokeWidth;

        layerDrawable.setLayerInset(1, strokeWidth, strokeWidth, strokeWidth, strokeWidth);
        ViewCompat.setBackground(this, layerDrawable);
    }

    public void putText(String s) {
        int textColor = mTextView.getCurrentTextColor();

        if (s.equals("0")) {
            mTextView.setTextColor(Color.argb(
                    0,
                    Color.red(textColor),
                    Color.green(textColor),
                    Color.blue(textColor))
            );
        } else {
            mTextView.setTextColor(Color.argb(
                    255,
                    Color.red(textColor),
                    Color.green(textColor),
                    Color.blue(textColor))
            );
        }

        mTextView.setText(s);
    }

    public void clearValue() {

        if (!mCellData.getSolved()) {
            updateEntered(0);
        }
    }

    public void solveGiven() {

        if (mCellData.getGiven()) {
            mCellData.setEnteredToAnswer();
            mCellData.setSolved(true);
            this.setGiven();
            putText(String.valueOf(mCellData.getAnswer()));
        }
    }

    public void updateEntered(int i) {
        putText(String.valueOf(i));
        mCellData.setEntered(i);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
