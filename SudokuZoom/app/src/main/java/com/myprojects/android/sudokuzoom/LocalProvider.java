/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.myprojects.android.sudokuzoom;

import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class LocalProvider extends ContentProvider {

    // The URI Matcher used by this content provider.
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private LocalDbHelper mOpenHelper;

    static final int GAME_INFO = 100;
    static final int GAME_INFO_WITH_START_TIME = 101;
    static final int TIMED_GAME_PUZZLE_INFO = 200;
    static final int TIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID = 201;
    static final int UNTIMED_GAME_PUZZLE_INFO = 202;
    static final int UNTIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID = 203;
    static final int BEST_TIMES = 300;
    static final int BEST_TIMES_WITH_LAYER_COUNT = 301;

    /*
        Students: Here is where you need to create the UriMatcher. This UriMatcher will
        match each URI to the WEATHER, WEATHER_WITH_LOCATION, WEATHER_WITH_LOCATION_AND_DATE,
        and LOCATION integer constants defined above.  You can test this by uncommenting the
        testUriMatcher test within TestUriMatcher.
     */
    static UriMatcher buildUriMatcher() {
        // I know what you're thinking.  Why create a UriMatcher when you can use regular
        // expressions instead?  Because you're not crazy, that's why.

        // All paths added to the UriMatcher have a corresponding code to return when a match is
        // found.  The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = LocalContract.CONTENT_AUTHORITY;

        // For each type of URI you want to add, create a corresponding code.
        matcher.addURI(authority, LocalContract.PATH_GAME_INFO, GAME_INFO);
        matcher.addURI(authority, LocalContract.PATH_GAME_INFO + "/*", GAME_INFO_WITH_START_TIME);
        matcher.addURI(authority, LocalContract.PATH_TIMED_GAME_PUZZLE_INFO, TIMED_GAME_PUZZLE_INFO);
        matcher.addURI(authority, LocalContract.PATH_TIMED_GAME_PUZZLE_INFO + "/*", TIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID);
        matcher.addURI(authority, LocalContract.PATH_UNTIMED_GAME_PUZZLE_INFO, UNTIMED_GAME_PUZZLE_INFO);
        matcher.addURI(authority, LocalContract.PATH_UNTIMED_GAME_PUZZLE_INFO + "/*", UNTIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID);
        matcher.addURI(authority, LocalContract.PATH_BEST_TIMES, BEST_TIMES);
        matcher.addURI(authority, LocalContract.PATH_BEST_TIMES + "/*", BEST_TIMES_WITH_LAYER_COUNT);

        return matcher;
    }

    /*
        Students: We've coded this for you.  We just create a new WeatherDbHelper for later use
        here.
     */
    @Override
    public boolean onCreate() {
        mOpenHelper = new LocalDbHelper(getContext());
        return true;
    }

    /*
        Students: Here's where you'll code the getType function that uses the UriMatcher.  You can
        test this by uncommenting testGetType in TestProvider.

     */
    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {

            case GAME_INFO:
                return LocalContract.GameInfoEntry.CONTENT_TYPE;
            case GAME_INFO_WITH_START_TIME:
                return LocalContract.GameInfoEntry.CONTENT_ITEM_TYPE;
            case TIMED_GAME_PUZZLE_INFO:
                return LocalContract.TimedGamePuzzleInfoEntry.CONTENT_TYPE;
            case TIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID:
                return LocalContract.TimedGamePuzzleInfoEntry.CONTENT_ITEM_TYPE;
            case UNTIMED_GAME_PUZZLE_INFO:
                return LocalContract.UntimedGamePuzzleInfoEntry.CONTENT_TYPE;
            case UNTIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID:
                return LocalContract.UntimedGamePuzzleInfoEntry.CONTENT_ITEM_TYPE;
            case BEST_TIMES:
                return LocalContract.BestTimesEntry.CONTENT_TYPE;
            case BEST_TIMES_WITH_LAYER_COUNT:
                return LocalContract.BestTimesEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        String table;

        // Here's the switch statement that, given a URI, will determine what kind of request it is,

        switch (sUriMatcher.match(uri)) {

            case GAME_INFO: {
                table =  LocalContract.GameInfoEntry.TABLE_NAME;
                break;
            }

            case GAME_INFO_WITH_START_TIME: {
                table = LocalContract.GameInfoEntry.TABLE_NAME;
                break;
            }

            case TIMED_GAME_PUZZLE_INFO: {
                table = LocalContract.TimedGamePuzzleInfoEntry.TABLE_NAME;
                break;
            }

            case TIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID: {
                table = LocalContract.TimedGamePuzzleInfoEntry.TABLE_NAME;
                break;
            }

            case UNTIMED_GAME_PUZZLE_INFO: {
                table = LocalContract.UntimedGamePuzzleInfoEntry.TABLE_NAME;
                break;
            }

            case UNTIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID: {
                table = LocalContract.UntimedGamePuzzleInfoEntry.TABLE_NAME;
                break;
            }

            case BEST_TIMES: {
                table = LocalContract.BestTimesEntry.TABLE_NAME;
                break;
            }

            case BEST_TIMES_WITH_LAYER_COUNT: {
                table = LocalContract.BestTimesEntry.TABLE_NAME;
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        return mOpenHelper.getReadableDatabase().query(
                table,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {

            case GAME_INFO: {
                long _id = db.insert(LocalContract.GameInfoEntry.TABLE_NAME, null, values);

                if ( _id > 0 )
                    returnUri = LocalContract.GameInfoEntry.buildSoloGameInfoUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);

                break;
            }

            case TIMED_GAME_PUZZLE_INFO: {
                long _id = db.insert(LocalContract.TimedGamePuzzleInfoEntry.TABLE_NAME, null, values);

                if ( _id > 0 )
                    returnUri = LocalContract.TimedGamePuzzleInfoEntry.buildTimedGamePuzzleInfoUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);

                break;
            }

            case UNTIMED_GAME_PUZZLE_INFO: {
                long _id = db.insert(LocalContract.UntimedGamePuzzleInfoEntry.TABLE_NAME, null, values);

                if ( _id > 0 )
                    returnUri = LocalContract.UntimedGamePuzzleInfoEntry.buildUntimedGamePuzzleInfoUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);

                break;
            }

            case BEST_TIMES: {
                long _id = db.insert(LocalContract.BestTimesEntry.TABLE_NAME, null, values);

                if ( _id > 0 )
                    returnUri = LocalContract.BestTimesEntry.buildBestTimesUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);

                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;

        switch (match) {

            case GAME_INFO:
                rowsDeleted = db.delete(
                        LocalContract.GameInfoEntry.TABLE_NAME,
                        selection,
                        selectionArgs
                );
                break;

            case GAME_INFO_WITH_START_TIME:
                rowsDeleted = db.delete(
                        LocalContract.GameInfoEntry.TABLE_NAME,
                        LocalContract.GameInfoEntry.COLUMN_START_TIME + " = " +
                                LocalContract.GameInfoEntry.getStartTimeFromUri(uri),
                        null
                );
                break;

            case TIMED_GAME_PUZZLE_INFO:
                rowsDeleted = db.delete(
                        LocalContract.TimedGamePuzzleInfoEntry.TABLE_NAME,
                        null,
                        null);
                break;

            case TIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID:
                rowsDeleted = db.delete(
                        LocalContract.TimedGamePuzzleInfoEntry.TABLE_NAME,
                        LocalContract.TimedGamePuzzleInfoEntry.COLUMN_PUZZLE_ID + " = " +
                                LocalContract.TimedGamePuzzleInfoEntry.getPuzzleIdFromUri(uri),
                        null);
                break;

            case UNTIMED_GAME_PUZZLE_INFO:
                rowsDeleted = db.delete(
                        LocalContract.UntimedGamePuzzleInfoEntry.TABLE_NAME,
                        null,
                        null);
                break;

            case UNTIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID:
                rowsDeleted = db.delete(
                        LocalContract.UntimedGamePuzzleInfoEntry.TABLE_NAME,
                        LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_PUZZLE_ID + " = " +
                                LocalContract.UntimedGamePuzzleInfoEntry.getPuzzleIdFromUri(uri),
                        null);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        return rowsDeleted;
    }

    @Override
    public int update(
            Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {

            case GAME_INFO_WITH_START_TIME:
                rowsUpdated = db.update(LocalContract.GameInfoEntry.TABLE_NAME,
                        values,
                        LocalContract.GameInfoEntry.COLUMN_START_TIME + " = " +
                                LocalContract.GameInfoEntry.getStartTimeFromUri(uri),
                        null);
                break;

            case TIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID:
                rowsUpdated = db.update(LocalContract.TimedGamePuzzleInfoEntry.TABLE_NAME,
                        values,
                        LocalContract.TimedGamePuzzleInfoEntry.COLUMN_PUZZLE_ID + " = " +
                                LocalContract.TimedGamePuzzleInfoEntry.getPuzzleIdFromUri(uri),
                        null);
                break;

            case UNTIMED_GAME_PUZZLE_INFO_WITH_PUZZLE_ID:
                rowsUpdated = db.update(LocalContract.UntimedGamePuzzleInfoEntry.TABLE_NAME,
                        values,
                        LocalContract.UntimedGamePuzzleInfoEntry.COLUMN_PUZZLE_ID + " = " +
                                LocalContract.UntimedGamePuzzleInfoEntry.getPuzzleIdFromUri(uri),
                        null);
                break;

            case BEST_TIMES_WITH_LAYER_COUNT:
                rowsUpdated = db.update(LocalContract.BestTimesEntry.TABLE_NAME,
                        values,
                        LocalContract.BestTimesEntry.COLUMN_LAYER_COUNT + " = " +
                                LocalContract.BestTimesEntry.getLayerCountFromUri(uri),
                        null);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        return 0;
    }

    // You do not need to call this method. This is a method specifically to assist the testing
    // framework in running smoothly. You can read more at:
    // http://developer.android.com/reference/android/content/ContentProvider.html#shutdown()
    @Override
    @TargetApi(11)
    public void shutdown() {
        mOpenHelper.close();
        super.shutdown();
    }
}