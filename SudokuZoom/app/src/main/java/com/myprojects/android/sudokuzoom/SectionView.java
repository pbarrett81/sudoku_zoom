package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;

/**
 * Created by Owner on 9/20/2017.
 */

public class SectionView extends RecyclerView {

    public static final String LOG_TAG = SectionView.class.getSimpleName();

    private Context mContext;

    private boolean mDisplayed = false;

    public SectionView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public SectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public SectionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    public void init() {
        final SectionView sectionView = this;

        sectionView.setLayoutManager(new GridLayoutManager(mContext, 3));
        sectionView.setAdapter(new SectionViewAdapter(new int[9]));

        sectionView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {

                        if (!mDisplayed) {
                            sectionView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            display();
                            mDisplayed = true;
                        }
                    }
                });
    }

    public void display() {
        SectionViewAdapter sectionViewAdapter = (SectionViewAdapter) getAdapter();
        sectionViewAdapter.setStrokeWidths();
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return false;
    }
}
