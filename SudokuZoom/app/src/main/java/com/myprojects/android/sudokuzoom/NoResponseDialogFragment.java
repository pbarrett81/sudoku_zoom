package com.myprojects.android.sudokuzoom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Owner on 11/19/2017.
 */

public class NoResponseDialogFragment extends DialogFragment {

    public static final int GENERATE_DIALOG = 0;
    public static final int LOAD_DIALOG = 1;

    private Context mContext;

    private AlertDialog mAlertDialog;

    public interface Callback {
        void onNoResponseOkSelected();
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.title_no_response);

        Bundle args = getArguments();

        if (args != null) {

            switch (args.getInt("DIALOG")) {

                case GENERATE_DIALOG:
                    builder.setMessage(R.string.message_no_response_create);
                    builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {
                            AppCompatActivity activity = (AppCompatActivity) getActivity();

                            if (activity != null) {
                                ((NoResponseDialogFragment.Callback) activity).onNoResponseOkSelected();
                            }

                            dismiss();
                        }
                    });
                    break;

                case LOAD_DIALOG:
                    builder.setMessage(R.string.message_no_response_load);
                    builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {
                            dismiss();
                        }
                    });
                    break;
            }
        }




        mAlertDialog = builder.create();

        return mAlertDialog;
    }
}
