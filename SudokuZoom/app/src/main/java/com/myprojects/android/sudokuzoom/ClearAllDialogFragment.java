package com.myprojects.android.sudokuzoom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

/**
 * Created by Owner on 11/19/2017.
 */

public class ClearAllDialogFragment extends DialogFragment {

    private Context mContext;

    private AlertDialog mAlertDialog;

    public interface Callback {
        void onConfirmYesPressed();
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.warning_sign);
        builder.setMessage(R.string.confirm_clear_message);
        builder.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dismiss();
                ((Callback) mContext).onConfirmYesPressed();
            }
        });
        builder.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dismiss();
            }
        });

        mAlertDialog = builder.create();

        return mAlertDialog;
    }
}
