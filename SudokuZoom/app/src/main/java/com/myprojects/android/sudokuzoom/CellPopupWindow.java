package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.PopupWindow;

public class CellPopupWindow extends PopupWindow {

    private NumberPicker mNumberPicker;

    private Button mOkButton;

    private int mPopupWidth;
    private int mPopupHeight;

    public interface Callback {
        void onPopupOkPressed(int num);
    }

    public CellPopupWindow(final Context context) {
        super(context);

        LinearLayout popupWindowLayout = (LinearLayout) LayoutInflater.from(context).inflate(
                R.layout.layout_popup_window,
                null
        );

        mNumberPicker = popupWindowLayout.findViewById(R.id.numberpicker);
        mNumberPicker.setMinValue(0);
        mNumberPicker.setMaxValue(9);
        mNumberPicker.setDisplayedValues(context.getResources().getStringArray(R.array.cell_values_array));

        mOkButton = (Button) popupWindowLayout.findViewById(R.id.button_ok);
        mOkButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dismiss();
                ((Callback) context).onPopupOkPressed(mNumberPicker.getValue());
            }
        });

        popupWindowLayout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        mPopupWidth = popupWindowLayout.getMeasuredWidth();
        mPopupHeight = popupWindowLayout.getMeasuredHeight();

        this.setContentView(popupWindowLayout);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public int getPopupWidth() {
        return mPopupWidth;
    }

    public int getPopupHeight() {
        return mPopupHeight;
    }

    public void setNumberPickerValue(int num) {
        mNumberPicker.setValue(num);
    }
}
