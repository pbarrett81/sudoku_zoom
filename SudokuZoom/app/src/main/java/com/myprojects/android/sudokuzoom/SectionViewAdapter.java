package com.myprojects.android.sudokuzoom;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Owner on 9/20/2017.
 */

public class SectionViewAdapter extends RecyclerView.Adapter<SectionViewAdapter.ViewHolder> {

    public static final String LOG_TAG = SectionViewAdapter.class.getSimpleName();

    private final int STANDARD_SECTION_WIDTH = 231;

    private int[] mDataset;
    private ArrayList<ViewHolder> mViewHolders = new ArrayList<>();

    private int mSectionColor;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private SectionLayout mSectionLayout;

        public ViewHolder(SectionLayout v) {
            super(v);
            mSectionLayout = v;
        }

        public SectionLayout getSectionLayout() {
            return mSectionLayout;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SectionViewAdapter(int[] myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SectionViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        SectionLayout v = new SectionLayout(parent.getContext());
        v.setBackgroundResource(R.drawable.background_section);

        ViewHolder vh = new ViewHolder(v);
        mViewHolders.add(vh);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mSectionLayout.init();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public void setSectionColor(int sectionColor) {
        mSectionColor = sectionColor;
    }

    public void setStrokeWidths() {
        float scale = mViewHolders.get(0).mSectionLayout.determineScale();

        for (int i = 0; i < mDataset.length; ++i) {
            mViewHolders.get(i).mSectionLayout.setStrokeProperties(scale, mSectionColor);
        }
    }

    public int getOpeningWidth() {
        return mViewHolders.get(0).mSectionLayout.calculateOpeningWidth();
    }

    public void runScaleAnimations() {

        for (SectionViewAdapter.ViewHolder vh : mViewHolders) {
            vh.getSectionLayout().runScaleAnimation();
        }
    }
}
