package com.myprojects.android.sudokuzoom;

import android.content.Context;
import android.os.Handler;
import android.widget.TextView;

import java.lang.ref.WeakReference;

/**
 * Created by Owner on 1/11/2018.
 */

public class GameTimer {

    private Handler mHandler;
    private Runnable mRunnable;

    private long mStartTime = 0;
    private long mElapsedTime = 0;
    private boolean mRun = false;

    private WeakReference<TextView> mTimerValue;

    public GameTimer() {
        mHandler = new Handler();
        mRunnable = new Runnable() {

            public void run() {

                if (mRun) {
                    mElapsedTime += 1000;
                    updateTimerValue();
                    mHandler.postDelayed(this, 1000);
                }
            }
        };
    }

    public long getElapsedTime() {
        return mElapsedTime;
    }

    public void setElapsedTime(long elapsedTime) {
        mElapsedTime = elapsedTime;
    }

    public long getStartTime() {
        return mStartTime;
    }

    public void setStartTime(long startTime) {
        mStartTime = startTime;
    }

    public void setTimerValue(TextView timerValue) {
        mTimerValue = new WeakReference<>(timerValue);
        updateTimerValue();
    }

    public void start() {
        mRun = true;
        mHandler.postDelayed(mRunnable, 1000);
    }

    public void stop() {
        mRun = false;
    }

    public void addElapsedToStart() {
        mStartTime += mElapsedTime;
    }

    public static String buildTimeString(Context context, long time, boolean addLeadingZeroes) {
        long elapsedTime = time / 1000;
        long minutes = elapsedTime / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        String strSeconds;
        String strMinutes;
        String strHours;
        String strDays;

        if (addLeadingZeroes) {
            strSeconds = Utils.addLeadingZeroes(String.valueOf(elapsedTime % 60), 2);
            strMinutes = Utils.addLeadingZeroes(String.valueOf(minutes % 60), 2);
            strHours = Utils.addLeadingZeroes(String.valueOf(hours % 24), 2);
            strDays = Utils.addLeadingZeroes(String.valueOf(days), 3);
        } else {
            strSeconds = String.valueOf(elapsedTime % 60);
            strMinutes = String.valueOf(minutes % 60);
            strHours = String.valueOf(hours % 24);
            strDays = String.valueOf(days);
        }

        return String.format(
                context.getString(R.string.time_format),
                strDays,
                strHours,
                strMinutes,
                strSeconds
        );
    }

    public void readyInitialDisplay() {

        if (mStartTime > 0) {
            mStartTime += mElapsedTime;
            mElapsedTime = 0;
            updateTimerValue();
        }
    }

    public void updateTimerValue() {

        if (mTimerValue != null) {
            TextView timerValue = mTimerValue.get();

            if (timerValue != null) {
                timerValue.setText(buildTimeString(timerValue.getContext(), mElapsedTime, true));
            }
        }
    }
}