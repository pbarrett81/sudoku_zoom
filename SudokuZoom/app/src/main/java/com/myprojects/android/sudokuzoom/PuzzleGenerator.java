package com.myprojects.android.sudokuzoom;

import org.paidaki.sudoku.grids.Sudoku9x9;
import org.paidaki.sudoku.model.Cell;
import org.paidaki.sudoku.model.Sudoku;
import org.paidaki.sudoku.model.enums.SudokuValue;
import org.paidaki.sudoku.solver.Solver;
import org.paidaki.sudoku.strategies.Strategy;
import org.paidaki.sudoku.strategies.StrategyFactory;

import java.util.ArrayList;
import java.util.Arrays;

import dlx.DancingLinks;
import dlx.SudokuDLX;

/**
 * Created by Owner on 11/7/2017.
 */

public final class PuzzleGenerator {

    private static final int CONSTRAINT_ROWS = 729;
    private static final int CONSTRAINT_COLS = 324;

    private static int[][] mConstraintMatrix = new int[CONSTRAINT_ROWS][CONSTRAINT_COLS];
    private static int[][] mSudoku = new int[9][9];
    private static int[][] mSudokuAnswer;

    public static boolean initPuzzleData(PuzzleData puzzleData) {
        generatePuzzle();

        String strSudoku = Arrays.deepToString(mSudoku);
        strSudoku = strSudoku.replaceAll("\\D+","");

        Sudoku sudoku = new Sudoku9x9();

        for (int i = 0; i < sudoku.getFullSize(); i++) {
            Cell cell = sudoku.getCells()[i];
            cell.setNewValue(SudokuValue.getValueByIndex(Character.getNumericValue(strSudoku.charAt(i))));
        }

        Solver solver = new Solver();
        Strategy strategy = solver.findHardestStrategy(sudoku);
        //Utils.log("strategy.getName()", strategy.getName());

        int strategyId = strategy.getId();

        //Utils.log("strategyId", strategyId);

        if (strategyId == StrategyFactory.FULL_HOUSE
                || strategyId == StrategyFactory.HIDDEN_SINGLE
                || strategyId == StrategyFactory.NAKED_SINGLE) {
            puzzleData.setSudoku(mSudoku);
            puzzleData.setAnswer(mSudokuAnswer);
            return true;
        }

        return false;

        /*createConstraintMatrix();
        createPuzzle();*/

        /*puzzleData.setSudoku(mSudoku);
        puzzleData.setAnswer(mSudokuAnswer);*/
    }

    public static void generatePuzzle() {
        createConstraintMatrix();
        createPuzzle();
    }

    private static void createConstraintMatrix() {
        final int C1_BASE = 0;
        final int C2_BASE = 81;
        final int C3_BASE = 162;
        final int C4_BASE = 243;

        int[] currRow = new int[CONSTRAINT_COLS];
        int c1 = C1_BASE;
        int c2 = C2_BASE;
        int c3 = C3_BASE;
        int c4 = C4_BASE;
        int lim1 = 0;
        int lim2 = 0;
        int lim3 = 0;

        ArrayList<String> freeRows = new ArrayList<String>();

        for (int m = 0; m < CONSTRAINT_ROWS; ++m) {
            freeRows.add(String.valueOf(m));
        }

        for (int i = 0; i < CONSTRAINT_ROWS; ++i) {

            for (int j = 0; j < currRow.length; ++j) {

                if (j == c1 || j == c2 || j == c3 || j == c4) {
                    currRow[j] = 1;
                } else {
                    currRow[j] = 0;
                }
            }

            if ((i + 1) % 9 == 0) {
                c1++;
            }

            if ((i + 1) % 81 == 0) {
                lim1 += 9;
            }

            if ((c2 + 1) % 9 == 0) {
                c2 = C2_BASE + lim1;
            } else {
                c2++;
            }

            if ((c3 + 1) % 81 == 0) {
                c3 = C3_BASE;
            } else {
                c3++;
            }

            if ((i + 1) % 27 == 0) {

                if (lim2 < 18) {
                    lim2 += 9;
                } else {
                    lim2 = 0;
                }
            }

            if ((i + 1) % 243 == 0) {
                lim3 += 27;
            }

            if ((c4 + 1) % 9 == 0) {
                c4 = C4_BASE + lim2 + lim3;
            } else {
                c4++;
            }

            String rand = (String) Utils.removeRandElement(freeRows);

            if (rand != null) {
                int randInt = Integer.parseInt(rand);
                System.arraycopy(currRow, 0, mConstraintMatrix[randInt], 0, currRow.length);
            }
        }
    }

    private static void createPuzzle() {
        MySudokuHandler mySudokuHandler = new MySudokuHandler(9);
        DancingLinks dlxSudoku = new DancingLinks(mConstraintMatrix, mySudokuHandler);
        dlxSudoku.runSolver();

        mSudokuAnswer = mySudokuHandler.getSudoku();

        for (int i = 0; i < mSudokuAnswer.length; ++i) {
            System.arraycopy(mSudokuAnswer[i], 0, mSudoku[i], 0, mSudokuAnswer[i].length);
        }

        ArrayList<String> coordList = Utils.getCoordinateList();
        int currX = 0;
        int currY = 0;
        int currV = 0;
        SudokuDLX sudokuDLX = new SudokuDLX();

        while (coordList.size() > 0) {
            String rand = (String) Utils.removeRandElement(coordList);

            if (rand != null) {
                String[] coords = rand.split(",");
                currX = Integer.parseInt(coords[0]) - 1;
                currY = Integer.parseInt(coords[1]) - 1;
                currV = mSudoku[currX][currY];
                mSudoku[currX][currY] = 0;
            }

            DancingLinks dlxPuzzle = new DancingLinks(sudokuDLX.makeExactCoverGrid(mSudoku));
            dlxPuzzle.runSolver();

            if (dlxPuzzle.getSolutions() != 1) {
                mSudoku[currX][currY] = currV;
            }
        }
    }
}


