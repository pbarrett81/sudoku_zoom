package dlx;

abstract class AbstractSudokuSolver{
    
    protected int S = 9; // size of the board
    protected int side = 3; // how long the side is
    
    // prints out all possible solutions. returns false if `sudoku` is invalid
    // to be implemented by concrete classes
    protected abstract void runSolver(int[][] sudoku);
    
    public static void printSolution(int[][] result){
        int N = result.length;
        for(int i = 0; i < N; i++){
            String ret = "";
            for(int j = 0; j < N; j++){
                ret += result[i][j] + " ";
            }
            //System.out.println(ret);
        }
        //System.out.println();
    }
}
