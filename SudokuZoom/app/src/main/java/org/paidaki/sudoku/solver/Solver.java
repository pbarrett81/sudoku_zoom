package org.paidaki.sudoku.solver;

import org.paidaki.sudoku.model.Sudoku;
import org.paidaki.sudoku.strategies.Strategy;
import org.paidaki.sudoku.strategies.StrategyFactory;

public class Solver {

    private static final Strategy STRATEGIES[] = {
            StrategyFactory.getStrategy(StrategyFactory.FULL_HOUSE),
            StrategyFactory.getStrategy(StrategyFactory.HIDDEN_SINGLE),
            StrategyFactory.getStrategy(StrategyFactory.NAKED_SINGLE),
            StrategyFactory.getStrategy(StrategyFactory.LOCKED_CANDIDATES_1),
            StrategyFactory.getStrategy(StrategyFactory.LOCKED_CANDIDATES_2),
            StrategyFactory.getStrategy(StrategyFactory.HIDDEN_PAIR),
            StrategyFactory.getStrategy(StrategyFactory.HIDDEN_TRIPLE),
            StrategyFactory.getStrategy(StrategyFactory.HIDDEN_QUADRUPLE)
            //TODO Add more strategies
    };

    public Strategy findHardestStrategy(Sudoku originalSudoku) {
        Sudoku sudoku = originalSudoku.clone();
        Strategy hardestStrategy = null;
        int currentStrategyIndex = 0;

        while (currentStrategyIndex < STRATEGIES.length) {
            Strategy strategy = STRATEGIES[currentStrategyIndex];

            if (strategy.applyStrategy(sudoku)) {
                hardestStrategy = (hardestStrategy == null || strategy.compareTo(hardestStrategy) >= 0) ? strategy : hardestStrategy;
                currentStrategyIndex = 0;
            } else {
                currentStrategyIndex++;
            }
        }
        return hardestStrategy;
    }
}
